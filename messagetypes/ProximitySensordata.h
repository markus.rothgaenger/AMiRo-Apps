/*
AMiRo-Apps is a collection of applications for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2021  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    ProximitySensordata.h
 * @brief   Data type describing the payload of the eight ring sensors.
 *
 * @addtogroup ProximitySensorData
 * @{
 */

#ifndef PROXIMITYSENSORDATA_H
#define PROXIMITYSENSORDATA_H

/******************************************************************************/
/* CONSTANTS                                                                  */
/******************************************************************************/

/******************************************************************************/
/* SETTINGS                                                                   */
/******************************************************************************/

/******************************************************************************/
/* CHECKS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* DATA STRUCTURES AND TYPES                                                  */
/******************************************************************************/

/**
 * @brief   Union to represent vcnl ring values.
 */
typedef union {
  uint16_t data[8];
  struct {
    uint16_t nnw;
    uint16_t wnw;
    uint16_t wsw;
    uint16_t ssw;
    uint16_t sse;
    uint16_t ese;
    uint16_t ene;
    uint16_t nne;
  }values;
} ring_sensors_t;

/**
 * @brief   Structure to handle the ring sensor data.
 */
typedef struct ring_data {
  ring_sensors_t values;
}ring_data_t;

/******************************************************************************/
/* MACROS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* EXTERN DECLARATIONS                                                        */
/******************************************************************************/

/******************************************************************************/
/* INLINE FUNCTIONS                                                           */
/******************************************************************************/

/******************************************************************************/
/* SUBSYSTEMS                                                                 */
/******************************************************************************/


#endif /* PROXIMITYSENSORDATA_H */
