/*
AMiRo-Apps is a collection of applications for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2021  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    CAN_IDs.h
 * @brief   Data type describing the payload of the accelerometer data.
 *
 * @addtogroup CAN_IDs
 * @{
 */


#ifndef CAN_IDS_H
#define CAN_IDS_H

/******************************************************************************/
/* CONSTANTS                                                                  */
/******************************************************************************/

#define CAN_TOPIC_DATA    1
#define CAN_REQUEST_DATA  1

/*
 * IDs of the different data which can be send over the CAN Bus
 */
#define CAN_FLOOR_PROX  1
#define CAN_FLOOR_AMB   2
#define CAN_GYRO        4

/******************************************************************************/
/* SETTINGS                                                                   */
/******************************************************************************/

/******************************************************************************/
/* CHECKS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* DATA STRUCTURES AND TYPES                                                  */
/******************************************************************************/

/**
 * @brief General structure to send data over the CAN Bus
 */
typedef struct can_data {
  uint16_t id; //Specifies of which type the data are (Gyroscope, Accelerometer, Proximity,...). Also used as Topic
  uint16_t data[8];
}can_data_t;

/******************************************************************************/
/* MACROS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* EXTERN DECLARATIONS                                                        */
/******************************************************************************/

/******************************************************************************/
/* INLINE FUNCTIONS                                                           */
/******************************************************************************/

/******************************************************************************/
/* SUBSYSTEMS                                                                 */
/******************************************************************************/

#endif /* DWD_ACCELEROMETER_H */
