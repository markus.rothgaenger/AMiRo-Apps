/*
AMiRo-Apps is a collection of applications for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2021  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    helloworld.h
 * @brief   A simple "Hello world!" application.
 *
 * @addtogroup HelloWorld
 * @{
 */

#ifndef HELLOWORLD_H
#define HELLOWORLD_H

#include <urt.h>

/******************************************************************************/
/* CONSTANTS                                                                  */
/******************************************************************************/

#if (URT_CFG_PUBSUB_ENABLED == true) || defined(__DOXYGEN__)

#if !defined(HELLOWORLD_TOPICID) || defined(__DOXYGEN__)
/**
 * @brief   Default topic identifier.
 */
#define HELLOWORLD_TOPICID                      123
#endif /* !defined(HELLOWORLD_TOPICID) */

/**
 * @brief   Event flag to identify trigger events related to publish-subscribe.
 */
#define HELLOWORLD_TRIGGERFLAG_PUBSUB           (urt_osEventFlags_t)(1 << 0)

#endif /* (URT_CFG_PUBSUB_ENABLED == true) */

#if (URT_CFG_RPC_ENABLED == true) || defined(__DOXYGEN__)

#if !defined(HELLOWORLD_SERVICEID) || defined(__DOXYGEN__)
/**
 * @brief   Default service identifier.
 */
#define HELLOWORLD_SERVICEID                    123
#endif /* !defined(HELLOWORLD_SERVICEID) */

/**
 * @brief   Event flag to identify trigger events related to remote procedure calls.
 */
#define HELLOWORLD_TRIGGERFLAG_RPC              (urt_osEventFlags_t)(1 << 1)

#endif /* (URT_CFG_RPC_ENABLED == true) */

/******************************************************************************/
/* SETTINGS                                                                   */
/******************************************************************************/

/******************************************************************************/
/* CHECKS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* DATA STRUCTURES AND TYPES                                                  */
/******************************************************************************/

/******************************************************************************/
/* MACROS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* EXTERN DECLARATIONS                                                        */
/******************************************************************************/

#if defined(__cplusplus)
extern "C" {
#endif /* defined(__cplusplus) */

#if ((URT_CFG_PUBSUB_ENABLED == true) || (URT_CFG_RPC_ENABLED == true)) || defined(__DOXYGEN__)
  float helloworldSrtUsefulness(urt_delay_t dt, void* params);
#endif /* (URT_CFG_PUBSUB_ENABLED == true) || (URT_CFG_RPC_ENABLED == true) */

#if defined(__cplusplus)
}
#endif /* defined(__cplusplus) */

/******************************************************************************/
/* INLINE FUNCTIONS                                                           */
/******************************************************************************/

/******************************************************************************/
/* SUBSYSTEMS                                                                 */
/******************************************************************************/

#include <helloworld_master.h>
#include <helloworld_slave.h>

#endif /* HELLOWORLD_H */

/** @} */
