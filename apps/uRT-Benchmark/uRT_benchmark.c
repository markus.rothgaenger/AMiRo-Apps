/*
AMiRo-Apps is a collection of applications for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2021  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    uRT_benchmark.c
 * @brief   A µRT benchmark application.
 *
 * @addtogroup uRT_Benchmark
 * @{
 */

#include "uRT_benchmark.h"

/******************************************************************************/
/* LOCAL DEFINITIONS                                                          */
/******************************************************************************/

#define TRIGGEREVENT                              (urtCoreGetEventMask() << 1)

#define TOPICEVENT                                (urtCoreGetEventMask() << 2)

#define REQUESTEVENT                              (urtCoreGetEventMask() << 3)

#define SERVICEEVENT                              (urtCoreGetEventMask() << 4)

/******************************************************************************/
/* EXPORTED VARIABLES                                                         */
/******************************************************************************/

/******************************************************************************/
/* LOCAL TYPES                                                                */
/******************************************************************************/

/******************************************************************************/
/* LOCAL VARIABLES                                                            */
/******************************************************************************/

/******************************************************************************/
/* LOCAL FUNCTIONS                                                            */
/******************************************************************************/

#if (URT_CFG_PUBSUB_ENABLED == true)



#endif /* (URT_CFG_PUBSUB_ENABLED == true) */

urt_osEventMask_t _ubnSetup(urt_node_t* node, void* bn)
{
  urtDebugAssert(bn != NULL);

  // local variables
  urtbenchmark_node_t* const benchmark = (urtbenchmark_node_t*)bn;
  urt_osEventMask_t waitevents = 0;

  // initialize node data
#if (CH_CFG_USE_REGISTRY == TRUE)
  // set thread name
  chRegSetThreadName(benchmark->config->node.name);
#endif /* (CH_CFG_USE_REGISTRY == TRUE) */
  if (benchmark->config->node.trigger != NULL) {
    urtEventListenerInit(&benchmark->config->node.listener);
    urtEventRegister(benchmark->config->node.trigger, &benchmark->config->node.listener, TRIGGEREVENT, 0);
    waitevents |= TRIGGEREVENT;
  }

#if (URT_CFG_PUBSUB_ENABLED == true)
  // initialize publisher data
  for (size_t p = 0; p < benchmark->config->publishers.size; ++p) {
    urtPublisherInit(&benchmark->config->publishers.buffer[p].publisher,
                     urtCoreGetTopic(benchmark->config->publishers.buffer[p].topicid));
  }

  // initialize subscriber data
  for (size_t s = 0; s < benchmark->config->subscribers.size; ++s) {
    switch (benchmark->config->subscribers.buffer[s].rtclass) {
      case URT_NRT:
        urtNrtSubscriberInit(&benchmark->config->subscribers.buffer[s].nrtsubscriber.subscriber);
        urtNrtSubscriberSubscribe(&benchmark->config->subscribers.buffer[s].nrtsubscriber.subscriber,
                                  urtCoreGetTopic(benchmark->config->subscribers.buffer[s].topicid),
                                  TOPICEVENT);
        break;
      case URT_SRT:
        urtSrtSubscriberInit(&benchmark->config->subscribers.buffer[s].srtsubscriber.subscriber);
        urtSrtSubscriberSubscribe(&benchmark->config->subscribers.buffer[s].srtsubscriber.subscriber,
                                  urtCoreGetTopic(benchmark->config->subscribers.buffer[s].topicid),
                                  TOPICEVENT,
                                  benchmark->config->subscribers.buffer[s].srtsubscriber.usefulnesscb,
                                  &benchmark->config->subscribers.buffer[s].srtsubscriber.params);
        break;
      case URT_FRT:
        urtFrtSubscriberInit(&benchmark->config->subscribers.buffer[s].frtsubscriber.subscriber);
        urtFrtSubscriberSubscribe(&benchmark->config->subscribers.buffer[s].frtsubscriber.subscriber,
                                  urtCoreGetTopic(benchmark->config->subscribers.buffer[s].topicid),
                                  TOPICEVENT,
                                  benchmark->config->subscribers.buffer[s].frtsubscriber.deadline,
                                  benchmark->config->subscribers.buffer[s].frtsubscriber.jitter);
        break;
      case URT_HRT:
        urtHrtSubscriberInit(&benchmark->config->subscribers.buffer[s].hrtsubscriber.subscriber);
        urtHrtSubscriberSubscribe(&benchmark->config->subscribers.buffer[s].hrtsubscriber.subscriber,
                                  urtCoreGetTopic(benchmark->config->subscribers.buffer[s].topicid),
                                  TOPICEVENT,
                                  benchmark->config->subscribers.buffer[s].hrtsubscriber.deadline,
                                  benchmark->config->subscribers.buffer[s].hrtsubscriber.jitter,
                                  benchmark->config->subscribers.buffer[s].hrtsubscriber.rate);
        break;
    }
    waitevents |= TOPICEVENT;
  }
#endif /* (URT_CFG_PUBSUB_ENABLED == true) */

#if (URT_CFG_RPC_ENABLED == true)
  // initialize service data
  for (size_t s = 0; s < benchmark->config->services.size; ++s) {
    urtServiceInit(&benchmark->config->services.buffer[s].service,
                   benchmark->config->services.buffer[s].serviceid,
                   ((urt_node_t*)node)->thread,
                   REQUESTEVENT);
    waitevents |= REQUESTEVENT;
  }

  // initialize request data
  for (size_t r = 0; r < benchmark->config->requests.size; ++r) {
    switch (benchmark->config->requests.buffer[r].rtclass) {
      case URT_NRT:
        urtNrtRequestInit(&benchmark->config->requests.buffer[r].nrtrequest.request,
                          benchmark->config->requests.buffer[r].payload.buffer);
        break;
      case URT_SRT:
        urtSrtRequestInit(&benchmark->config->requests.buffer[r].srtrequest.request,
                          benchmark->config->requests.buffer[r].payload.buffer);
        break;
      case URT_FRT:
        urtFrtRequestInit(&benchmark->config->requests.buffer[r].frtrequest.request,
                          benchmark->config->requests.buffer[r].payload.buffer,
                          benchmark->config->requests.buffer[r].frtrequest.jitter);
        break;
      case URT_HRT:
        urtHrtRequestInit(&benchmark->config->requests.buffer[r].hrtrequest.request,
                          benchmark->config->requests.buffer[r].payload.buffer,
                          benchmark->config->requests.buffer[r].hrtrequest.jitter);
        break;
    }
    // retrieve the service
    while (true) {
      benchmark->config->requests.buffer[r].service = urtCoreGetService(benchmark->config->requests.buffer[r].serviceid);
      if (benchmark->config->requests.buffer[r].service != NULL) {
        break;
      } else {
        urtThreadMSleep(1);
      }
    }
  }
#endif /* (URT_CFG_RPC_ENABLED == true) */

  return waitevents;
}

urt_osEventMask_t _ubnLoop(urt_node_t* node, urt_osEventMask_t events, void* bn)
{
  urtDebugAssert(bn != NULL);
  (void)node;

  // local variables
  urtbenchmark_node_t* const benchmark = (urtbenchmark_node_t*)bn;
  urt_osEventMask_t waitevents = 0;

  // handle node benchmark
  if (events & TRIGGEREVENT && benchmark->config->node.time != NULL) {
    // track latency
    urt_osTime_t t;
    urtTimeNow(&t);
    *benchmark->config->node.latencybuffer = urtTimeDiff(benchmark->config->node.time, &t);
    ++benchmark->config->node.latencybuffer;
    waitevents |= TRIGGEREVENT;
  }

#if (URT_CFG_PUBSUB_ENABLED == true)
  // handle publisher benchmark
  if (events & TRIGGEREVENT) {
    // publish a new message for all publishers
    for (size_t p = 0; p < benchmark->config->publishers.size; ++p) {
      if (benchmark->config->publishers.buffer[p].time != NULL) {
        urtPublisherPublish(&benchmark->config->publishers.buffer[p].publisher,
                            benchmark->config->publishers.buffer[p].payload.buffer,
                            benchmark->config->publishers.buffer[p].payload.size,
                            benchmark->config->publishers.buffer[p].time,
                            benchmark->config->publishers.buffer[p].policy);
      } else {
        urt_osTime_t t;
        urtTimeNow(&t);
        urtPublisherPublish(&benchmark->config->publishers.buffer[p].publisher,
                            benchmark->config->publishers.buffer[p].payload.buffer,
                            benchmark->config->publishers.buffer[p].payload.size,
                            &t,
                            benchmark->config->publishers.buffer[p].policy);
      }
      waitevents |= TRIGGEREVENT;
    }
  }

  // handle subscriber benchmark
  if (events & TOPICEVENT) {
    // fetch all pending messages for all subscribers
    for (size_t s = 0; s < benchmark->config->subscribers.size; ++s) {
      urt_osTime_t origintime;
      urtTimeSet(&origintime, 0);
      switch (benchmark->config->subscribers.buffer[s].rtclass) {
        case URT_NRT:
          while (urtNrtSubscriberFetchNextMessage(&benchmark->config->subscribers.buffer[s].nrtsubscriber.subscriber,
                                                  benchmark->config->subscribers.buffer[s].payload.buffer,
                                                  &benchmark->config->subscribers.buffer[s].payload.size,
                                                  &origintime,
                                                  benchmark->config->subscribers.buffer[s].latencybuffer)
                 == URT_STATUS_OK) {
            ++benchmark->config->subscribers.buffer[s].latencybuffer;
          }
          break;
        case URT_SRT:
          while (urtSrtSubscriberFetchNextMessage(&benchmark->config->subscribers.buffer[s].srtsubscriber.subscriber,
                                                  benchmark->config->subscribers.buffer[s].payload.buffer,
                                                  &benchmark->config->subscribers.buffer[s].payload.size,
                                                  &origintime,
                                                  benchmark->config->subscribers.buffer[s].latencybuffer)
                 == URT_STATUS_OK) {
            ++benchmark->config->subscribers.buffer[s].latencybuffer;
          }
          break;
        case URT_FRT:
          while (urtFrtSubscriberFetchNextMessage(&benchmark->config->subscribers.buffer[s].frtsubscriber.subscriber,
                                                  benchmark->config->subscribers.buffer[s].payload.buffer,
                                                  &benchmark->config->subscribers.buffer[s].payload.size,
                                                  &origintime,
                                                  benchmark->config->subscribers.buffer[s].latencybuffer)
                 == URT_STATUS_OK) {
            ++benchmark->config->subscribers.buffer[s].latencybuffer;
          }
          break;
        case URT_HRT:
          while (urtHrtSubscriberFetchNextMessage(&benchmark->config->subscribers.buffer[s].hrtsubscriber.subscriber,
                                                  benchmark->config->subscribers.buffer[s].payload.buffer,
                                                  &benchmark->config->subscribers.buffer[s].payload.size,
                                                  &origintime,
                                                  benchmark->config->subscribers.buffer[s].latencybuffer)
                 == URT_STATUS_OK) {
            ++benchmark->config->subscribers.buffer[s].latencybuffer;
          }
          break;
      }
      waitevents |= TOPICEVENT;
    }
  }
#endif /* (URT_CFG_PUBSUB_ENABLED == true) */

#if (URT_CFG_RPC_ENABLED == true)
  if (events & TRIGGEREVENT) {
    // submit all requests
    for (size_t r = 0; r < benchmark->config->requests.size; ++r) {
      urtDebugAssert(benchmark->config->requests.buffer[r].service != NULL);

      /* for each request:
       * 1) acquire request
       * 2) set payload
       * 3) submit reuqest
       */
      switch (benchmark->config->requests.buffer[r].rtclass) {
        case URT_NRT:
          if (urtNrtRequestAcquire(&benchmark->config->requests.buffer[r].nrtrequest.request) != URT_STATUS_OK) {
            urtNrtRequestRetrieve(&benchmark->config->requests.buffer[r].nrtrequest.request, URT_REQUEST_RETRIEVE_ENFORCING, NULL, NULL);
          }
          memset(urtNrtRequestGetPayload(&benchmark->config->requests.buffer[r].nrtrequest.request),
                 benchmark->config->requests.buffer[r].payload.size % 0x0100,
                 benchmark->config->requests.buffer[r].payload.size);
          urtNrtRequestSubmit(&benchmark->config->requests.buffer[r].nrtrequest.request,
                              benchmark->config->requests.buffer[r].service,
                              benchmark->config->requests.buffer[r].payload.size,
                              SERVICEEVENT);
          break;
        case URT_SRT:
          if (urtSrtRequestAcquire(&benchmark->config->requests.buffer[r].srtrequest.request) != URT_STATUS_OK) {
            urtSrtRequestRetrieve(&benchmark->config->requests.buffer[r].srtrequest.request, URT_REQUEST_RETRIEVE_ENFORCING, NULL,NULL);
          }
          memset(urtSrtRequestGetPayload(&benchmark->config->requests.buffer[r].srtrequest.request),
                 benchmark->config->requests.buffer[r].payload.size % 0x0100,
                 benchmark->config->requests.buffer[r].payload.size);
          urtSrtRequestSubmit(&benchmark->config->requests.buffer[r].srtrequest.request,
                              benchmark->config->requests.buffer[r].service,
                              benchmark->config->requests.buffer[r].payload.size,
                              SERVICEEVENT);
          break;
        case URT_FRT:
          if (urtFrtRequestAcquire(&benchmark->config->requests.buffer[r].frtrequest.request) != URT_STATUS_OK) {
            urtFrtRequestRetrieve(&benchmark->config->requests.buffer[r].frtrequest.request, URT_REQUEST_RETRIEVE_ENFORCING, NULL,NULL);
          }
          memset(urtFrtRequestGetPayload(&benchmark->config->requests.buffer[r].frtrequest.request),
                 benchmark->config->requests.buffer[r].payload.size % 0x0100,
                 benchmark->config->requests.buffer[r].payload.size);
          urtFrtRequestSubmit(&benchmark->config->requests.buffer[r].frtrequest.request,
                              benchmark->config->requests.buffer[r].service,
                              benchmark->config->requests.buffer[r].payload.size,
                              SERVICEEVENT,
                              benchmark->config->requests.buffer[r].frtrequest.deadline);
          break;
        case URT_HRT:
          if (urtHrtRequestAcquire(&benchmark->config->requests.buffer[r].hrtrequest.request) != URT_STATUS_OK) {
            urtHrtRequestRetrieve(&benchmark->config->requests.buffer[r].hrtrequest.request, URT_REQUEST_RETRIEVE_ENFORCING, NULL,NULL);
          }
          memset(urtHrtRequestGetPayload(&benchmark->config->requests.buffer[r].hrtrequest.request),
                 benchmark->config->requests.buffer[r].payload.size % 0x0100,
                 benchmark->config->requests.buffer[r].payload.size);
          urtHrtRequestSubmit(&benchmark->config->requests.buffer[r].hrtrequest.request,
                              benchmark->config->requests.buffer[r].service,
                              benchmark->config->requests.buffer[r].payload.size,
                              SERVICEEVENT,
                              benchmark->config->requests.buffer[r].hrtrequest.deadline);
          break;
      }
      waitevents |= TRIGGEREVENT | SERVICEEVENT;
    }
  }

  if (events & REQUESTEVENT) {
    // dispatch and process all requests for each service
    for (size_t s = 0; s < benchmark->config->services.size; ++s) {
      size_t size = 0;
      bool noreturn = false;
      while (true) {
        // dispatch next request
        urt_service_dispatched_t dispatched = urtServiceDispatch(&benchmark->config->services.buffer[s].service,
                                                                 benchmark->config->services.buffer[s].payload.buffer,
                                                                 &size,
                                                                 &noreturn);
        // if ther are no pending requests
        if (!urtServiceDispatchedIsValid(&dispatched)) {
          // break the loop
          break;
        }
        // track dispatch latency
        if (benchmark->config->services.buffer[s].latencybuffer != NULL) {
          urt_osTime_t t;
          urtTimeNow(&t);
          *benchmark->config->services.buffer[s].latencybuffer = urtTimeDiff(&dispatched.submissionTime, &t);
        }
        // induce artificial delay if requested
        if (benchmark->config->services.buffer[s].delay != NULL) {
          urtThreadUSleep(*benchmark->config->services.buffer[s].delay);
        }
        if (benchmark->config->services.buffer[s].latencybuffer != NULL) {
          ++benchmark->config->services.buffer[s].latencybuffer;
        }
        // acquire request and respond
        if (urtServiceAcquireRequest(&benchmark->config->services.buffer[s].service, &dispatched) == URT_STATUS_OK) {
          memcpy(urtRequestGetPayload(dispatched.request),
                 benchmark->config->services.buffer[s].payload.buffer,
                 size);
          urtServiceRespond(&dispatched, size);
        }
      }
    }
    waitevents |= REQUESTEVENT;
  }

  if (events & SERVICEEVENT) {
    // retrieve all requests
    for (size_t r = 0; r < benchmark->config->requests.size; ++r) {
      /* for each request:
       * 1) chech whether request is pending
       * 2) try to retrieve the request
       * 3) check whether the request could be retrieved
       * 4) release request
       */
      size_t size = 0;
      switch (benchmark->config->requests.buffer[r].rtclass) {
        case URT_NRT:
          if (urtNrtRequestIsPending(&benchmark->config->requests.buffer[r].nrtrequest.request)) {
            urtNrtRequestRetrieve(&benchmark->config->requests.buffer[r].nrtrequest.request,
                                  benchmark->config->requests.buffer[r].policy,
                                  &size,
                                  benchmark->config->requests.buffer[r].latencybuffer);
            if (!urtNrtRequestIsPending(&benchmark->config->requests.buffer[r].nrtrequest.request)) {
              urtNrtRequestRelease(&benchmark->config->requests.buffer[r].nrtrequest.request);
              ++benchmark->config->requests.buffer[r].latencybuffer;
            }
          }
          break;
        case URT_SRT:
          if (urtSrtRequestIsPending(&benchmark->config->requests.buffer[r].srtrequest.request)) {
            urtSrtRequestRetrieve(&benchmark->config->requests.buffer[r].srtrequest.request,
                                  benchmark->config->requests.buffer[r].policy,
                                  &size,
                                  benchmark->config->requests.buffer[r].latencybuffer);
            if (!urtSrtRequestIsPending(&benchmark->config->requests.buffer[r].srtrequest.request)) {
              urtSrtRequestRelease(&benchmark->config->requests.buffer[r].srtrequest.request);
              ++benchmark->config->requests.buffer[r].latencybuffer;
            }
          }
          break;
        case URT_FRT:
          if (urtFrtRequestIsPending(&benchmark->config->requests.buffer[r].frtrequest.request)) {
            urtFrtRequestRetrieve(&benchmark->config->requests.buffer[r].frtrequest.request,
                                  benchmark->config->requests.buffer[r].policy,
                                  &size,
                                  benchmark->config->requests.buffer[r].latencybuffer);
            if (!urtFrtRequestIsPending(&benchmark->config->requests.buffer[r].frtrequest.request)) {
              urtFrtRequestRelease(&benchmark->config->requests.buffer[r].frtrequest.request);
              ++benchmark->config->requests.buffer[r].latencybuffer;
            }
          }
          break;
        case URT_HRT:
          if (urtHrtRequestIsPending(&benchmark->config->requests.buffer[r].hrtrequest.request)) {
            urtHrtRequestRetrieve(&benchmark->config->requests.buffer[r].hrtrequest.request,
                                  benchmark->config->requests.buffer[r].policy,
                                  &size,
                                  benchmark->config->requests.buffer[r].latencybuffer);
            if (!urtHrtRequestIsPending(&benchmark->config->requests.buffer[r].hrtrequest.request)) {
              urtHrtRequestRelease(&benchmark->config->requests.buffer[r].hrtrequest.request);
              ++benchmark->config->requests.buffer[r].latencybuffer;
            }
          }
          break;
      }
      waitevents |= TRIGGEREVENT | SERVICEEVENT;
    }
  }
#endif /* (URT_CFG_RPC_ENABLED == true) */

  return waitevents;
}

void _ubnShutdown(urt_node_t* node, urt_status_t reason, void* bn)
{
  urtDebugAssert(bn != NULL);
  (void)node;
  (void)reason;

  // local variables
  urtbenchmark_node_t* const benchmark = (urtbenchmark_node_t*)bn;

  // shutdown node
  if (benchmark->config->node.trigger != NULL) {
    urtEventUnregister(benchmark->config->node.trigger, &benchmark->config->node.listener);
  }

#if (URT_CFG_PUBSUB_ENABLED == true)
  // shutdown publishers: nothing to do

  // shutdown subscribers
  for (size_t s = 0; s < benchmark->config->subscribers.size; ++s) {
    switch (benchmark->config->subscribers.buffer[s].rtclass) {
      case URT_NRT:
        urtNrtSubscriberUnsubscribe(&benchmark->config->subscribers.buffer[s].nrtsubscriber.subscriber);
        break;
      case URT_SRT:
        urtSrtSubscriberUnsubscribe(&benchmark->config->subscribers.buffer[s].srtsubscriber.subscriber);
        break;
      case URT_FRT:
        urtFrtSubscriberUnsubscribe(&benchmark->config->subscribers.buffer[s].frtsubscriber.subscriber);
        break;
      case URT_HRT:
        urtHrtSubscriberUnsubscribe(&benchmark->config->subscribers.buffer[s].hrtsubscriber.subscriber);
        break;
    }
  }
#endif /* (URT_CFG_PUBSUB_ENABLED == true) */

#if (URT_CFG_RPC_ENABLED == true)
  // shutdown requests: nothing to do

  // shutdown services: nothing to do
#endif /* (URT_CFG_RPC_ENABLED == true) */

  return;
}

/******************************************************************************/
/* EXPORTED FUNCTIONS                                                         */
/******************************************************************************/

void ubnInit(urtbenchmark_node_t* bn, urt_osThreadPrio_t prio, urtbenchmark_config_t* config)
{
  urtDebugAssert(bn != NULL);
  urtDebugAssert(config != NULL);

  // initialize the node
  urtNodeInit(&bn->node, (urt_osThread_t*)bn->thread, sizeof(bn->thread), prio,
              _ubnSetup, bn,
              _ubnLoop, bn,
              _ubnShutdown, bn);

  // set the configuration
  bn->config = config;

  return;
}

void ubnConfigInit(urtbenchmark_config_t* config)
{
  // reset node data
#if (CH_CFG_USE_REGISTRY == TRUE)
    memset(config->node.name, '\0', sizeof(config->node.name));
#endif /* (CH_CFG_USE_REGISTRY == TRUE) */
    config->node.trigger = NULL;
    config->node.time = NULL;
    config->node.latencybuffer = NULL;

#if (URT_CFG_PUBSUB_ENABLED == true)
    // reset publisher data
    config->publishers.buffer = NULL;
    config->publishers.size = 0;

    // reset subscriber data
    config->subscribers.buffer = NULL;
    config->subscribers.size = 0;
#endif /* (URT_CFG_PUBSUB_ENABLED == true) */

#if (URT_CFG_RPC_ENABLED == true)
    // reset request data
    config->requests.buffer = NULL;
    config->requests.size = 0;

    // reset service data
    config->services.buffer = NULL;
    config->services.size = 0;
#endif /* (URT_CFG_RPC_ENABLED == true) */

  return;
}
