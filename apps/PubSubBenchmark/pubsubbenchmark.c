/*
AMiRo-Apps is a collection of applications for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2021  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    pubsubbenchmark.c
 * @brief   An application for testing the publish subscribe system.
 *
 * @addtogroup PubSubWorld
 * @{
 */

#include <pubsubbenchmark.h>
#include <amiroos.h>

/******************************************************************************/
/* LOCAL DEFINITIONS                                                          */
/******************************************************************************/

/**
 * @brief
 */
publisher_data_t nodes[2];

/**
 * @brief   PUBSUB (default) thread.
 */
URT_THREAD_MEMORY(_sub_thread, PUBSUB_STACKSIZE);

/**
 * @brief Subscriber which is subscribed to the topic where the publisher publish to
 */
static urt_nrtsubscriber_t nrtSubscriber;

/**
 * @brief topic where publisher publish to
 *
 */
static urt_topic_t pubsub_topic;

/**
 * @brief   Event mask for timer events
 * @details Event maks which is fired periodically by a timer and let all publisher publish
 */
#define PUBSUBWORLD_TIMERPUBLISHER_EVENTMASK              (urtCoreGetEventMask() << 1)


/**
 * @brief   Event mask for the subscriber
 */
#define PUBSUBWORLD_SUBSCRIBER_EVENTMASK                (urtCoreGetEventMask() << 2)

/**
 * @brief Event mask for the shell
 */
#define PUBSUBWORLD_SHELL_EVENTMASK                (urtCoreGetEventMask() << 3)

/**
 * @brief   Period for the periodic timer in microseconds.
 */
#define PUBSUBWORLD_TIMER_PERIOD                 MICROSECONDS_PER_SECOND*5

/******************************************************************************/
/* EXPORTED VARIABLES                                                         */
/******************************************************************************/

/******************************************************************************/
/* LOCAL TYPES                                                                */
/******************************************************************************/

/******************************************************************************/
/* LOCAL VARIABLES                                                            */
/******************************************************************************/

/******************************************************************************/
/* LOCAL FUNCTIONS                                                            */
/******************************************************************************/

/**
 * @brief   Callback function for the periodic timer.
 *
 * @param[in] src   Event source to fire.
 */
void _PubSubtimercb(void* src)
{
  chEvtBroadcastFlagsI((urt_osEventSource_t*)src, 0);
  return;
}


/**
 * @brief   Setup callback for the PubSubWorld node.
 * @details Initialize a publisher with PubSubWorlds topic.
*/
float usefulnesscb(urt_delay_t latency, void* cbparams) {
  (void)latency;
  (void)cbparams;

  return 1.0f;
}

/******************************************************************************/
/* EXPORTED FUNCTIONS                                                         */
/******************************************************************************/


/**
 * @brief   Test functions of the nrt subscriber.
 *
 * @param[in] node  The node instance, which calls this callback.
 * @param[in] ps   The PubSubWorld instance to initialize.
 *
 * @return  Mask of events the node thread shall listen to.
 */
urt_osEventMask_t _publishernodesetup(urt_node_t* node, void* pup)
{
  urtDebugAssert(pup != NULL);
  (void)node;

#if (URT_CFG_DEBUG_ENABLED == true)
  urtThreadUSleep(PUBSUBWORLD_TIMER_PERIOD);
  urtPrintf("PubSubWorld setup callback of the publisher...\n");
#endif /* (URT_CFG_DEBUG_ENABLED == true) */

  //Initialize a message
  urtMessageInit(&((publisher_data_t*)pup)->message_publish, NULL);

  // Initialize a new publisher with one empty message and pubsubworlds topic
  urtPublisherInit(&((publisher_data_t*)pup)->publisher, &pubsub_topic , &((publisher_data_t*)pup)->message_publish);

  // Initialize a new publisher with pubsubworlds topic
//  urtPublisherInit(&((publisher_data_t*)pup)->publisher, &pubsub_topic , NULL);

  //Initialize the Event Listener
  urtEventListenerInit(&((publisher_data_t*)pup)->timer_evtlis);
  //Register events
  urtEventRegister(&((publisher_data_t*)pup)->pubsubData->timer_evtsrc, &((publisher_data_t*)pup)->timer_evtlis, PUBSUBWORLD_TIMERPUBLISHER_EVENTMASK, 0);

  return PUBSUBWORLD_TIMERPUBLISHER_EVENTMASK;
}

/**
 * @brief   Loop callback for a PubSub node.
 * @detail  Let all Publisher publish
 *
 * @param[in] node    The node instance, which calls this callback.
 * @param[in] event   Mask of the latest event recieved.
 * @param[in] ps      The PubSubWorld data structure of this node.
 *
 * @return Mask with event the node thread shall listen to.
 */
urt_osEventMask_t _publishernodeloop(urt_node_t* node, urt_osEventMask_t event, void* pup)
{
  urtDebugAssert(pup != NULL);
  (void)node;
  (void)event;

  urt_osTime_t publishTime = urtTimeNow();
  urtPublisherPublish(&((publisher_data_t*)pup)->publisher, NULL, 0, publishTime);

  return urtCoreGetEventMask() | PUBSUBWORLD_TIMERPUBLISHER_EVENTMASK;
}

/**
 * @brief   Setup callback for the PubSubWorld node.
 * @details Initialize a subscriber
 *
 * @param[in] node  The node instance, which calls this callback.
 * @param[in] ps   The PubSubWorld instance to initialize.
 *
 * @return  Mask of events the node thread shall listen to.
 */
urt_osEventMask_t _subscribernodesetup(urt_node_t* node, void* ps)
{
  urtDebugAssert(ps != NULL);
  (void)node;

#if (URT_CFG_DEBUG_ENABLED == true)
  urtThreadUSleep(PUBSUBWORLD_TIMER_PERIOD);
  urtPrintf("PubSubWorld setup callback of the subscriber...\n");
#endif /* (URT_CFG_DEBUG_ENABLED == true) */

  //Initialize the NRT Subscriber
  urtNrtSubscriberInit (&nrtSubscriber);

  //Subscribe NRT Subscriber
  urtNrtSubscriberSubscribe(&nrtSubscriber, &pubsub_topic, PUBSUBWORLD_SUBSCRIBER_EVENTMASK, 0, NULL);

  return PUBSUBWORLD_SUBSCRIBER_EVENTMASK;
}

/**
 *
 * @param[in] node    The node instance, which calls this callback.
 * @param[in] event   Mask of the latest event recieved.
 * @param[in] ps      The PubSubWorld data structure of this node.
 *
 * @return  Mask with event the node thread shall listen to.
 */
urt_osEventMask_t _subscribernodeloop(urt_node_t* node, urt_osEventMask_t event, void* ps)
{
  (void)node;
  (void)event;
  (void)ps;

  static urt_delay_t latency;
  while (urtNrtSubscriberFetchNextMessage (&nrtSubscriber, NULL, 0, &latency) != URT_STATUS_FETCH_NOMESSAGE) {
    urtPrintf("latency: %i \n", latency);
  }
  urtPrintf("\n\n");

  return urtCoreGetEventMask() | PUBSUBWORLD_SUBSCRIBER_EVENTMASK;
}

/**
 * @brief   Empty node setup. Let every node listen to event throwed when the user starts the benchmark over the shell.
 *
 * @param[in] node  The node instance, which calls this callback.
 * @param[in] psn   The PubSubNode instance.
 *
 * @return  Mask of events the node thread shall listen to.
 */
urt_osEventMask_t _pubsubnodesetup(urt_node_t* node, void* psn)
{
  urtDebugAssert(psn != NULL);
  (void)node;

#if (URT_CFG_DEBUG_ENABLED == true)
  urtThreadUSleep(PUBSUBWORLD_TIMER_PERIOD);
  urtPrintf("Node setup callback...\n");
#endif /* (URT_CFG_DEBUG_ENABLED == true) */
  //Initialize the Event Listener
  urtEventListenerInit(&((publisher_data_t*)psn)->timer_evtlis);
  //Register events
  urtEventRegister(&((publisher_data_t*)psn)->pubsubData->timer_evtsrc, &((publisher_data_t*)psn)->timer_evtlis, PUBSUBWORLD_SHELL_EVENTMASK, 0);

  return urtCoreGetEventMask() | PUBSUBWORLD_SHELL_EVENTMASK;
}

/**
 * @brief   Loop callback for a PubSub node.
 * @detail  Let all Publisher publish
 *
 * @param[in] node    The node instance, which calls this callback.
 * @param[in] event   Mask of the latest event recieved.
 * @param[in] ps      The PubSubWorld data structure of this node.
 *
 * @return Mask with event the node thread shall listen to.
 */
urt_osEventMask_t _pubsubnodeloop(urt_node_t* node, urt_osEventMask_t event, void* psn)
{
  urtDebugAssert(psn != NULL);
  (void)node;
  (void)event;

  if (event == PUBSUBWORLD_SHELL_EVENTMASK) {
    //TODO: Check enum in publisher_data_t if this node should be a subscriber, a publisher or inactive (then do nothing)

    //Initialize a message
    urtMessageInit(&((publisher_data_t*)psn)->message_publish, NULL);

    // Initialize a new publisher with one empty message and pubsubworlds topic
    urtPublisherInit(&((publisher_data_t*)psn)->publisher, &pubsub_topic , &((publisher_data_t*)psn)->message_publish);

    //Unregister from shell event and register to timer event so that this node will listen to the timer event
    urtEventUnregister(&((publisher_data_t*)psn)->pubsubData->timer_evtsrc, &((publisher_data_t*)psn)->timer_evtlis);
    urtEventRegister(&((publisher_data_t*)psn)->pubsubData->timer_evtsrc, &((publisher_data_t*)psn)->timer_evtlis, PUBSUBWORLD_TIMERPUBLISHER_EVENTMASK, 0);

    return urtCoreGetEventMask() | PUBSUBWORLD_TIMERPUBLISHER_EVENTMASK;
  } else {
    urt_osTime_t publishTime = urtTimeNow();
    urtPublisherPublish(&((publisher_data_t*)psn)->publisher, NULL, 0, publishTime);

    return urtCoreGetEventMask() | PUBSUBWORLD_TIMERPUBLISHER_EVENTMASK;
  }
}


/******************************************************************************/
/* EXPORTED FUNCTIONS FOR TIME BENCHMARKING                                   */
/******************************************************************************/

/**
 * @brief mainBenchmarkNRTTime
 * @details Initializs a timer
 *
 * @param[in] ps
 */
void initializeTimer(pubsubworld_data_t* ps)
{
  // initialize all data
  urtTimerInit(&((pubsubworld_data_t*)ps)->timer);
  urtEventSourceInit(&((pubsubworld_data_t*)ps)->timer_evtsrc);

  return;
}

void startTimer(pubsubworld_data_t* ps, int numPubs, int numSubs, int numTopics, void *payload)
{
  //TODO: Enum in publisher_data_t if node is activ/publisher/subscriber
  //TODO: Go through array of nodes and set node to publisher/subscriber depending on numPubs, numSubs
  //TODO: numTopics > 0: numPubs =  numSubs = numTopics

  // start periodic timer
  urtTimerSetPeriodic(&((pubsubworld_data_t*)ps)->timer, PUBSUBWORLD_TIMER_PERIOD, _PubSubtimercb, &((pubsubworld_data_t*)ps)->timer_evtsrc);

  return;
}

void initializeTopic(void)
{
  //Initialize the topic where all publisher publish to
  urtTopicInit(&pubsub_topic, 0, NULL);

  return;
}

void initializeSub(pubsubworld_data_t* ps)
{
  //Initialize a node for one subscriber
  static urt_node_t _sub_node;
  urtNodeInit(&_sub_node, (urt_osThread_t*)_sub_thread, sizeof(_sub_thread), URT_THREAD_PRIO_NORMAL_MIN, _subscribernodesetup, ps, _subscribernodeloop, ps, NULL, NULL);

  return;
}

void initializeNode(pubsubworld_data_t* ps, int i)
{
  nodes[i].pubsubData = ps;
  urtNodeInit(&nodes[i].node, (urt_osThread_t*)nodes[i].thread, sizeof(nodes[i].thread), URT_THREAD_PRIO_NORMAL_MIN, _pubsubnodesetup, &nodes[i], _pubsubnodeloop, &nodes[i], NULL, NULL);
  return;
}

/**
 * @brief  Test the nrt subscriber.
 *
 * @return  void.
 */
void initializeNodesNrtSubscriber(pubsubworld_data_t* ps)
{
  urtDebugAssert(ps != NULL);

  //Initalize a timer
  initializeTimer(ps);

  initializeTopic();

  initializeSub(ps);

  //Initialize all nodes
  for (int nodeCount = 0; nodeCount < 2; nodeCount++) {
    initializeNode(ps,nodeCount);
  }

  return;
}
