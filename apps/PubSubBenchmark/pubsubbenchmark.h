/*
AMiRo-Apps is a collection of applications for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2021  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    pubsubbenchmark.h
 * @brief   An application for testing the publish subscribe system.
 *
 * @addtogroup PubSubWorld
 * @{
 */

#ifndef PUBSUBBENCHMARK_H
#define PUBSUBBENCHMARK_H

#include <urt.h>
#include <string.h>

/******************************************************************************/
/* CONSTANTS                                                                  */
/******************************************************************************/

/**
 * @brief   Stack size of the PubSubWorld application thread(s).
 */
#define PUBSUB_STACKSIZE                    256

/******************************************************************************/
/* SETTINGS                                                                   */
/******************************************************************************/

/******************************************************************************/
/* CHECKS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* DATA STRUCTURES AND TYPES                                                  */
/******************************************************************************/


/**
 * @brief   PubSubWorld benchmark application data structure.
 */
typedef struct pubsubworld_data {

  /**
   * @brief   Custom name to print during loop.
   */
  const char* name;

  /**
   * @brief   Timer for periodic loop execution.
   */
  urt_osTimer_t timer;

  /**
   * @brief   Event source for timer events.
   */
  urt_osEventSource_t timer_evtsrc;
} pubsubworld_data_t;


/**
 * @brief   Publisher benchmark application data structure.
 */
typedef struct publisher_data {
  /**
   * @brief   Event source for timer events.
   * @details Used to register the node to the timer event
   */
  pubsubworld_data_t* pubsubData;

  /**
   * @brief   PUBSUB (default) thread.
   */
  URT_THREAD_MEMORY(thread, PUBSUB_STACKSIZE);

  /**
   * @brief   Event listener for timer events.
   */
  urt_osEventListener_t timer_evtlis;

  /**
   * @brief   Publisher which should publish.
   */
  urt_publisher_t publisher;

  /**
   * @brief   Message which the publisher should publish.
   */
  urt_message_t message_publish;

  /**
   * @brief Node
   */
  urt_node_t node;
} publisher_data_t;

/******************************************************************************/
/* MACROS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* EXTERN DECLARATIONS                                                        */
/******************************************************************************/

#if defined(__cplusplus)
extern "C" {
#endif /* defined(__cplusplus) */
    void startTimer(pubsubworld_data_t* ps, int numPubs, int numSubs, int numTopics, void* payload);
    void initializeNodesNrtSubscriber(pubsubworld_data_t* ps);
#if defined(__cplusplus)
}
#endif /* defined(__cplusplus) */

/******************************************************************************/
/* INLINE FUNCTIONS                                                           */
/******************************************************************************/

#endif /* PUBSUBBENCHMARK_H */

/** @} */
