/*
AMiRo-Apps is a collection of applications for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2021  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    rpcbenchmark.c
 * @brief   An application for testing the Remote Procedure Call.
 *
 * @addtogroup RPC
 * @{
 */

#include <rpcbenchmark.h>
#include <amiroos.h>

/******************************************************************************/
/* LOCAL DEFINITIONS                                                          */
/******************************************************************************/

/**
 * @brief   Stack size of the RPC application thread(s).
 */
#define RPC_STACKSIZE                    256

/**
 * @brief   RPC (default) thread.
 */
URT_THREAD_MEMORY(_request_thread0, RPC_STACKSIZE);

/**
 * @brief   RPC (default) thread.
 */
URT_THREAD_MEMORY(_request_thread1, RPC_STACKSIZE);

/**
 * @brief   RPC (default) thread.
 */
URT_THREAD_MEMORY(_request_thread2, RPC_STACKSIZE);


/**
 * @brief   RPC (default) thread.
 */
URT_THREAD_MEMORY(_service_thread, RPC_STACKSIZE);

/**
 * @brief Service where all Request submit to.
 */
static urt_service_t rpc_service;


/**
 * @brief   Event mask for timer events
 * @details Event maks which is fired periodically by a timer and let all publisher publish
 */
#define RPC_TIMERPUBLISHER_EVENTMASK              (urtCoreGetEventMask() << 1)


/**
 * @brief   Event mask for the service
 */
#define RPC_SERVICE_EVENTMASK                (urtCoreGetEventMask() << 2)

/**
 * @brief   Period for the periodic timer in microseconds.
 */
#define RPC_TIMER_PERIOD                 MICROSECONDS_PER_SECOND*5

/******************************************************************************/
/* EXPORTED VARIABLES                                                         */
/******************************************************************************/

/******************************************************************************/
/* LOCAL TYPES                                                                */
/******************************************************************************/

/******************************************************************************/
/* LOCAL VARIABLES                                                            */
/******************************************************************************/

/******************************************************************************/
/* EXPORTED FUNCTIONS                                                         */
/******************************************************************************/

/**
 * @brief   Setup callback for the RPC node.
 * @details Initialize a request
 *
 * @param[in] node  The node instance, which calls this callback.
 * @param[in] pr   The RPC instance to initialize.
 *
 * @return  Mask of events the request thread shall listen to.
 */
urt_osEventMask_t _requestnodesetup(urt_node_t* node, void* req)
{
  urtDebugAssert(req != NULL);
  (void)node;

#if (URT_CFG_DEBUG_ENABLED == true)
  urtThreadUSleep(RPC_TIMER_PERIOD);
  urtPrintf("RPC setup callback of the request...\n");
  urtThreadSleep(0.01);
#endif /* (URT_CFG_DEBUG_ENABLED == true) */

  if (((request_data_t*)req)->rpcData->testRequest == nrt) {
    //Initialize the nrtRequest
    urtNrtRequestInit(&((request_data_t*)req)->nrtRequest);
  } else if (((request_data_t*)req)->rpcData->testRequest == srt) {
    //Initialize the srtRequest
    urtSrtRequestInit(&((request_data_t*)req)->srtRequest);
  } else if (((request_data_t*)req)->rpcData->testRequest == frt) {
    //Initialize the frtRequest
    urtFrtRequestInit(&((request_data_t*)req)->frtRequest);
  } else {
    //Initialize the hrtRequest
    urtHrtRequestInit(&((request_data_t*)req)->hrtRequest);
  }

  //Initialize the Event Listener
  urtEventListenerInit(&((request_data_t*)req)->timer_evtlis);
  //Register events
  urtEventRegister(&((request_data_t*)req)->rpcData->timer_evtsrc, &((request_data_t*)req)->timer_evtlis, RPC_TIMERPUBLISHER_EVENTMASK, 0);

  return RPC_TIMERPUBLISHER_EVENTMASK;
}

/**
 * @brief   Loop callback for a RPC node.
 * @detail  Let all Request Submit
 *
 * @param[in] node    The node instance, which calls this callback.
 * @param[in] event   Mask of the latest event recieved.
 * @param[in] req     The request data structure of this node.
 *
 * @return Mask with event the node thread shall listen to.
 */
urt_osEventMask_t _requestnodeloop(urt_node_t* node, urt_osEventMask_t event, void* req)
{
  urtDebugAssert(req != NULL);
  (void)node;
  (void)event;

  if (((request_data_t*)req)->rpcData->testRequest == nrt) {
    if (urtNrtRequestAcquire(&((request_data_t*)req)->nrtRequest) == URT_STATUS_OK) {
      urtNrtRequestSubmit(&((request_data_t*)req)->nrtRequest, &rpc_service);
    } else {
      urtPrintf("ERROR: RequestAcquire return status of request  %02x: BADOWNER \n", &((request_data_t*)req)->nrtRequest);
      urtThreadSleep(0.01);
    }
  } else if (((request_data_t*)req)->rpcData->testRequest == srt) {
    if (urtSrtRequestAcquire(&((request_data_t*)req)->srtRequest) == URT_STATUS_OK) {
      urtSrtRequestSubmit(&((request_data_t*)req)->srtRequest, &rpc_service);
    } else {
      urtPrintf("ERROR: RequestAcquire return status of request  %02x: BADOWNER \n", &((request_data_t*)req)->srtRequest);
      urtThreadSleep(0.01);
    }
  } else if (((request_data_t*)req)->rpcData->testRequest == frt) {
    if (urtFrtRequestAcquire(&((request_data_t*)req)->frtRequest) == URT_STATUS_OK) {
      urtFrtRequestSubmit(&((request_data_t*)req)->frtRequest, &rpc_service, 0);
    } else {
      urtPrintf("ERROR: RequestAcquire return status of request  %02x: BADOWNER \n", &((request_data_t*)req)->frtRequest);
      urtThreadSleep(0.01);
    }
  } else {  
    if (urtHrtRequestAcquire(&((request_data_t*)req)->hrtRequest) == URT_STATUS_OK) {
      urtHrtRequestSubmit(&((request_data_t*)req)->hrtRequest, &rpc_service, 0);
    } else {
      urtPrintf("ERROR: RequestAcquire return status of request  %02x: BADOWNER \n", &((request_data_t*)req)->hrtRequest);
      urtThreadSleep(0.01);
    }
  }

  return urtCoreGetEventMask() | RPC_TIMERPUBLISHER_EVENTMASK;
}

/**
 * @brief   Setup callback for the RPC node.
 * @details Initialize a service
 *
 * @param[in] node  The node instance, which calls this callback.
 * @param[in] pr   The RPC instance to initialize.
 *
 * @return  Mask of events the service thread shall listen to.
 */
urt_osEventMask_t _servicenodesetup(urt_node_t* node, void* pr)
{
  urtDebugAssert(pr != NULL);
  (void)node;

#if (URT_CFG_DEBUG_ENABLED == true)
  urtThreadUSleep(RPC_TIMER_PERIOD);
  urtPrintf("RPC setup callback of the service...\n");
  urtThreadSleep(0.01);
#endif /* (URT_CFG_DEBUG_ENABLED == true) */

  //Initialize the Service
  urtServiceInit (&rpc_service, 0, RPC_SERVICE_EVENTMASK, 0);

  return RPC_SERVICE_EVENTMASK;
}

/**
 *
 * @param[in] node    The node instance, which calls this callback.
 * @param[in] event   Mask of the latest event recieved.
 * @param[in] ps      The PubSubWorld data structure of this node.
 *
 * @return  Mask with event the node thread shall listen to.
 */
urt_osEventMask_t _servicenodeloop(urt_node_t* node, urt_osEventMask_t event, void* pr)
{
  (void)node;
  (void)event;
  (void)pr;

  urt_baserequest_t* dispatch_request = urtServiceDispatch(&rpc_service, NULL, 0);
  while (dispatch_request) {
    urtPrintf("request: %02x \n", dispatch_request);
    urtThreadSleep(0.01);
    if (urtServiceAcquireRequest(&rpc_service, dispatch_request) == URT_STATUS_OK) {
      urtServiceRespond(dispatch_request);
    } else {
      urtPrintf("ERROR: ServiceAcquireRequest return status is BADOWNER \n");
      urtThreadSleep(0.01);
    }
    dispatch_request = urtServiceDispatch(&rpc_service, NULL, 0);
  }
  urtPrintf("\n");

  return urtCoreGetEventMask() | RPC_SERVICE_EVENTMASK;
}

/******************************************************************************/
/* LOCAL FUNCTIONS                                                            */
/******************************************************************************/

/**
 * @brief   Callback function for the periodic timer.
 *
 * @param[in] src   Event source to fire.
 */
void _RPCtimercb(void* src)
{
  chEvtBroadcastFlagsI((urt_osEventSource_t*)src, 0);
  return;
}

/**
 * @brief   Initializes the service and request nodes.
 *
 * @param[in] pr   The RPC instance to initalizes the nodes for.
 */
void _initializeNodes(rpc_data_t* pr)
{
  //Initialize a node for the service
  static urt_node_t _service_node;
  urtNodeInit(&_service_node, (urt_osThread_t*)_service_thread, sizeof(_service_thread), URT_THREAD_PRIO_NORMAL_MIN, _servicenodesetup, pr, _servicenodeloop, pr, NULL, NULL);

  static request_data_t requestData0;
  requestData0.rpcData = pr;
  //Initialize a node for one request
  static urt_node_t _request_node0;
  urtNodeInit(&_request_node0, (urt_osThread_t*)_request_thread0, sizeof(_request_thread0), URT_THREAD_PRIO_NORMAL_MIN, _requestnodesetup, &requestData0, _requestnodeloop, &requestData0, NULL, NULL);

  static request_data_t requestData1;
  requestData1.rpcData = pr;
  //Initialize a node for one request
  static urt_node_t _request_node1;
  urtNodeInit(&_request_node1, (urt_osThread_t*)_request_thread1, sizeof(_request_thread1), URT_THREAD_PRIO_NORMAL_MIN, _requestnodesetup, &requestData1, _requestnodeloop, &requestData1, NULL, NULL);

  static request_data_t requestData2;
  requestData2.rpcData = pr;
  //Initialize a node for one request
  static urt_node_t _request_node2;
  urtNodeInit(&_request_node2, (urt_osThread_t*)_request_thread2, sizeof(_request_thread1), URT_THREAD_PRIO_NORMAL_MIN, _requestnodesetup, &requestData2, _requestnodeloop, &requestData2, NULL, NULL);

  return;
}

/******************************************************************************/
/* EXPORTED FUNCTIONS FOR BENCHMARKING                                        */
/******************************************************************************/

/**
 * @brief mainBenchmarkNRTTime
 * @details Initializs a timer
 *
 * @param[in] ps
 */
void initializeRPCTimer(rpc_data_t* pr)
{
  // initialize all data
  urtTimerInit(&((rpc_data_t*)pr)->timer);
  urtEventSourceInit(&((rpc_data_t*)pr)->timer_evtsrc);
  return;
}

void startRPCTimer(rpc_data_t* pr)
{
  // start periodic timer
  urtTimerSetPeriodic(&((rpc_data_t*)pr)->timer, RPC_TIMER_PERIOD, _RPCtimercb, &((rpc_data_t*)pr)->timer_evtsrc);
  return;
}

/**
  * @brief Test the nrt request.
  */
void testNRTRequest(rpc_data_t* pr)
{
  urtDebugAssert(pr != NULL);

  urtPrintf("Test the nrt Request \n");
  urtThreadSleep(0.5);

  pr->testRequest = nrt;

  _initializeNodes(pr);

  initializeRPCTimer(pr);
  startRPCTimer(pr);

  return;
}

/**
  * @brief Test the srt request.
  */
void testSRTRequest(rpc_data_t* pr)
{
  urtDebugAssert(pr != NULL);

  urtPrintf("Test the srt Request \n");
  urtThreadSleep(0.5);

  pr->testRequest = srt;

  _initializeNodes(pr);

  initializeRPCTimer(pr);
  startRPCTimer(pr);

  return;
}

/**
  * @brief Test the frt request.
  */
void testFRTRequest(rpc_data_t* pr)
{
  urtDebugAssert(pr != NULL);

  urtPrintf("Test the frt Request \n");
  urtThreadSleep(0.5);

  pr->testRequest = frt;

  _initializeNodes(pr);

  initializeRPCTimer(pr);
  startRPCTimer(pr);

  return;
}

/**
  * @brief Test the hrt request.
  */
void testHRTRequest(rpc_data_t* pr)
{
  urtDebugAssert(pr != NULL);

  urtPrintf("Test the hrt Request \n");
  urtThreadSleep(0.5);

  pr->testRequest = hrt;

  _initializeNodes(pr);

  initializeRPCTimer(pr);
  startRPCTimer(pr);

  return;
}
