/*
AMiRo-Apps is a collection of applications for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2021  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    rpcbenchmark.h
 * @brief   An application for testing the remote procedure call.
 *
 * @addtogroup RPCWorld
 * @{
 */

#ifndef RPCBENCHMARK_H
#define RPCBENCHMARK_H

#include <urt.h>
#include <string.h>

/******************************************************************************/
/* CONSTANTS                                                                  */
/******************************************************************************/

/******************************************************************************/
/* SETTINGS                                                                   */
/******************************************************************************/

/******************************************************************************/
/* CHECKS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* DATA STRUCTURES AND TYPES                                                  */
/******************************************************************************/

enum request {nrt, frt, srt, hrt};

/**
 * @brief   RPC benchmark application data structure.
 */
typedef struct rpc_data {

  /**
   * @brief   Custom name to print during loop.
   */
  const char* name;

  /**
   * @brief   Timer for periodic loop execution.
   */
  urt_osTimer_t timer;

  /**
   * @brief   Event source for timer events.
   */
  urt_osEventSource_t timer_evtsrc;

  /**
   * @brief Indicator which Request should be tested
   */
  int testRequest;
} rpc_data_t;

/**
 * @brief   Publisher benchmark application data structure.
 */
typedef struct request_data {

  /**
   * @brief   Event source for timer events.
   * @details Used to register the node to the timer event
   */
  rpc_data_t* rpcData;

  /**
   * @brief   Event listener for timer events.
   */
  urt_osEventListener_t timer_evtlis;

  /**
   * @brief   NrtRequest which should request.
   */
  urt_nrtrequest_t nrtRequest;

  /**
   * @brief   SrtRequest which should request.
   */
  urt_srtrequest_t srtRequest;

  /**
   * @brief   FrtRequest which should request.
   */
  urt_frtrequest_t frtRequest;

  /**
   * @brief   HrtRequest which should request.
   */
  urt_hrtrequest_t hrtRequest;
} request_data_t;


/******************************************************************************/
/* MACROS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* EXTERN DECLARATIONS                                                        */
/******************************************************************************/

#if defined(__cplusplus)
extern "C" {
#endif /* defined(__cplusplus) */
  void testNRTRequest(rpc_data_t* pr);
  void testFRTRequest(rpc_data_t* pr);
  void testSRTRequest(rpc_data_t* pr);
  void testHRTRequest(rpc_data_t* pr);
#if defined(__cplusplus)
}
#endif /* defined(__cplusplus) */

/******************************************************************************/
/* INLINE FUNCTIONS                                                           */
/******************************************************************************/

#endif /* RPCBENCHMARK_H */

/** @} */
