/*
AMiRo-Apps is a collection of applications for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2021  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <mazelogic.h>
#include <amiroos.h>

/******************************************************************************/
/* LOCAL DEFINITIONS                                                          */
/******************************************************************************/

/******************************************************************************/
/* EXPORTED VARIABLES                                                         */
/******************************************************************************/

/******************************************************************************/
/* LOCAL TYPES                                                                */
/******************************************************************************/

/******************************************************************************/
/* LOCAL VARIABLES                                                            */
/******************************************************************************/

/**
 * @brief   Name of ring nodes.
 */
static const char _logic_name[] = "MazeLogic";

/******************************************************************************/
/* LOCAL FUNCTIONS                                                            */
/******************************************************************************/

/**
 * @brief   Setup callback function for maze nodes.
 *
 * @param[in] node    Pointer to the node object.
 *                    Must not be NULL.
 * @param[in] maze    Pointer to the maze structure.
 *                    Must nor be NULL.
 *
 * @return  Event mask to listen for next.
 */
urt_osEventMask_t _mazeLogic_Setup(urt_node_t* node, void* maze)
{
  urtDebugAssert(maze != NULL);
  (void)node;

  // set thread name
  chRegSetThreadName(_logic_name);

#if (URT_CFG_PUBSUB_ENABLED == true)
  // subscribe to the floor proximity topic
  urt_topic_t* const floor_prox_topic = urtCoreGetTopic(((mazeLogic_node_t*)maze)->floor[0].topicid);
  urtDebugAssert(floor_prox_topic != NULL);
  urtNrtSubscriberSubscribe(&((mazeLogic_node_t*)maze)->floor[0].nrt, floor_prox_topic, TRIGGEREVENT, NULL);

  // subscribe to the floor ambient topic
  urt_topic_t* const floor_amb_topic = urtCoreGetTopic(((mazeLogic_node_t*)maze)->floor[1].topicid);
  urtDebugAssert(floor_amb_topic != NULL);
  urtNrtSubscriberSubscribe(&((mazeLogic_node_t*)maze)->floor[1].nrt, floor_amb_topic, TRIGGEREVENT, NULL);

  // subscribe to the ring proximity topic
  urt_topic_t* const ring_prox_topic = urtCoreGetTopic(((mazeLogic_node_t*)maze)->ring[0].topicid);
  urtDebugAssert(ring_prox_topic != NULL);
  urtNrtSubscriberSubscribe(&((mazeLogic_node_t*)maze)->ring[0].nrt, ring_prox_topic, TRIGGEREVENT, NULL);

  // subscribe to the ring ambient topic
  urt_topic_t* const ring_amb_topic = urtCoreGetTopic(((mazeLogic_node_t*)maze)->ring[1].topicid);
  urtDebugAssert(ring_amb_topic != NULL);
  urtNrtSubscriberSubscribe(&((mazeLogic_node_t*)maze)->ring[1].nrt, ring_amb_topic, TRIGGEREVENT, NULL);
#endif /* URT_CFG_PUBSUB_ENABLED == true */

  return TRIGGEREVENT;
}


/**
 * @brief   Loop callback function for maze nodes.
 *
 * @param[in] node    Pointer to the node object.
 *                    Must not be NULL.
 * @param[in] maze    Pointer to the maze structure.
 *                    Must nor be NULL.
 *
 * @return  Event mask to listen for next.
 */
urt_osEventMask_t _mazeLogic_Loop(urt_node_t* node, urt_osEventMask_t event, void* maze)
{
  urtDebugAssert(maze != NULL);

  (void)node;

  switch(event) {
    case TRIGGEREVENT:
    {
      // local variables
      urt_status_t floor_prox_status;
      urt_status_t ring_prox_status;

      // fetch NRT of the floor proximity data
      do {
        floor_prox_status = urtNrtSubscriberFetchNextMessage(&((mazeLogic_node_t*)maze)->floor[0].nrt, &((mazeLogic_node_t*)maze)->floor[0].data, sizeof(((mazeLogic_node_t*)maze)->floor[0].data), NULL, NULL);
      } while (floor_prox_status != URT_STATUS_FETCH_NOMESSAGE);

      // fetch NRT of the floor ambient data
      do {
        floor_prox_status = urtNrtSubscriberFetchNextMessage(&((mazeLogic_node_t*)maze)->floor[1].nrt, &((mazeLogic_node_t*)maze)->floor[1].data, sizeof(((mazeLogic_node_t*)maze)->floor[1].data), NULL, NULL);
      } while (floor_prox_status != URT_STATUS_FETCH_NOMESSAGE);

      // fetch NRT of the ring proximity data
      do {
        ring_prox_status = urtNrtSubscriberFetchNextMessage(&((mazeLogic_node_t*)maze)->ring[0].nrt, &((mazeLogic_node_t*)maze)->ring[0].data, sizeof(((mazeLogic_node_t*)maze)->ring[0].data), NULL, NULL);
      } while (ring_prox_status != URT_STATUS_FETCH_NOMESSAGE);

      // fetch NRT of the ring ambient data
      do {
        ring_prox_status = urtNrtSubscriberFetchNextMessage(&((mazeLogic_node_t*)maze)->ring[1].nrt, &((mazeLogic_node_t*)maze)->ring[1].data, sizeof(((mazeLogic_node_t*)maze)->ring[1].data), NULL, NULL);
      } while (ring_prox_status != URT_STATUS_FETCH_NOMESSAGE);
      break;
    }
  }

  return TRIGGEREVENT;
}

/**
 * @brief   Shutdown callback function for maze nodes.
 *
 * @param[in] node    Pointer to the node object.
 *                    Must not be NULL.
 * @param[in] maze    Pointer to the maze structure.
 *                    Must nor be NULL.
 */
void _mazeLogic_Shutdown(urt_node_t* node, urt_status_t reason, void* maze)
{
  urtDebugAssert(maze != NULL);

  (void)node;
  (void)reason;

#if (URT_CFG_PUBSUB_ENABLED == true)
  // unsubscribe topics
  urtNrtSubscriberUnsubscribe(&((mazeLogic_node_t*)maze)->ring[0].nrt);
  urtNrtSubscriberUnsubscribe(&((mazeLogic_node_t*)maze)->ring[1].nrt);
  urtNrtSubscriberUnsubscribe(&((mazeLogic_node_t*)maze)->floor[0].nrt);
  urtNrtSubscriberUnsubscribe(&((mazeLogic_node_t*)maze)->floor[1].nrt);
#endif /* (URT_CFG_PUBSUB_ENABLED == true) */

  return;
}

void printData (BaseSequentialStream* stream, mazeLogic_node_t* mazeLogic)
{
  for (int sensor_Floor = 0; sensor_Floor < 4; sensor_Floor++) {
    chprintf(stream, "Floor sensor: %u \t Proximity: %u \t ambient: %u \n",
             sensor_Floor,
             mazeLogic->floor[0].data.data[sensor_Floor],
             mazeLogic->floor[1].data.data[sensor_Floor]);
  }
  chprintf(stream, "\n");

  for (int sensor_Ring = 0; sensor_Ring < 8; sensor_Ring++) {
    chprintf(stream, "Ring sensor: %u \t Proximity: %u \t ambient: %u \n",
             sensor_Ring,
             mazeLogic->ring[0].data.data[sensor_Ring],
             mazeLogic->ring[1].data.data[sensor_Ring]);
  }
  return;
}

/******************************************************************************/
/* EXPORTED FUNCTIONS                                                         */
/******************************************************************************/


void mazeLogicInit(mazeLogic_node_t* mazeLogic, urt_osThreadPrio_t prio)
{
#if (URT_CFG_PUBSUB_ENABLED == true)
  urtNrtSubscriberInit(&mazeLogic->ring[0].nrt);
  urtNrtSubscriberInit(&mazeLogic->ring[1].nrt);
  urtNrtSubscriberInit(&mazeLogic->floor[0].nrt);
  urtNrtSubscriberInit(&mazeLogic->floor[1].nrt);
#endif /* URT_CFG_PUBSUB_ENABLED == true */

  // initialize the node
  urtNodeInit(&mazeLogic->node, (urt_osThread_t*)mazeLogic->thread, sizeof(mazeLogic->thread), prio,
              _mazeLogic_Setup, mazeLogic,
              _mazeLogic_Loop, mazeLogic,
              _mazeLogic_Shutdown, mazeLogic);

  return;
}
