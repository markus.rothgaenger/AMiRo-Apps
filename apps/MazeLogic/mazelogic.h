/*
AMiRo-Apps is a collection of applications for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2021  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MAZELOGIC_H
#define MAZELOGIC_H

#include <urt.h>
#include "../../messagetypes/ProximitySensordata.h"
#include "../../messagetypes/DWD_floordata.h"

/******************************************************************************/
/* CONSTANTS                                                                  */
/******************************************************************************/

#if !defined(MAZELOGIC_STACKSIZE) || defined(__DOXYGEN__)
/**
 * @brief   Stack size of maze logic threads.
 */
#define MAZELOGIC_STACKSIZE             256
#endif /* !defined(MAZELOGIC_STACKSIZE) */

/**
 * @brief   Event mask to set on a trigger event.
 */
#define TRIGGEREVENT                (urt_osEventMask_t)(1<< 1)

#define MAZE_TOPICID                  3 //Topic identifier for the maze logic topicid

/******************************************************************************/
/* SETTINGS                                                                   */
/******************************************************************************/

/******************************************************************************/
/* CHECKS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* DATA STRUCTURES AND TYPES                                                  */
/******************************************************************************/

/**
* @brief   Trigger related data of the floor sensors.
*/
typedef struct floor{
 urt_topicid_t topicid;
 urt_nrtsubscriber_t nrt;
 floor_sensors_t data;
} floor_t;

/**
 * @brief   Trigger related data of the ring sensors.
 */
typedef struct ring {
  urt_topicid_t topicid;
  urt_nrtsubscriber_t nrt;
  ring_sensors_t data;
} ring_t;

/**
 * @brief   MazeLogic node.
 * @struct  mazeLogic_node
 */
typedef struct mazeLogic_node {
  /**
   * @brief   Thread memory.
   */
  URT_THREAD_MEMORY(thread, MAZELOGIC_STACKSIZE);

  /**
   * @brief   Node object.
   */
  urt_node_t node;

  /**
   * @brief ambient and proximity data of the floor sensors
   * @details 0 = proximity, 1 = ambient
   */
  floor_t floor[2];

  /**
   * @brief ambient and proximity data of the ring sensors
   * @details 0 = proximity, 1 = ambient
   */
  ring_t ring[2];

} mazeLogic_node_t;

/******************************************************************************/
/* MACROS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* EXTERN DECLARATIONS                                                        */
/******************************************************************************/

#if defined(__cplusplus)
extern "C" {
#endif /* defined(__cplusplus) */
  void mazeLogicInit(mazeLogic_node_t* mazeLogic, urt_osThreadPrio_t prio);
  void printData (BaseSequentialStream* stream, mazeLogic_node_t* mazeLogic);
#if defined(__cplusplus)
}
#endif /* defined(__cplusplus) */

/******************************************************************************/
/* INLINE FUNCTIONS                                                           */
/******************************************************************************/

/******************************************************************************/
/* SUBSYSTEMS                                                                 */
/******************************************************************************/

#endif /* MAZELOGIC_H */

/** @} */
