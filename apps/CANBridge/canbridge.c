/*
AMiRo-Apps is a collection of applications for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2021  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <canbridge.h>
#include <amiroos.h>

/******************************************************************************/
/* LOCAL DEFINITIONS                                                          */
/******************************************************************************/

/******************************************************************************/
/* EXPORTED VARIABLES                                                         */
/******************************************************************************/

/******************************************************************************/
/* LOCAL TYPES                                                                */
/******************************************************************************/

/******************************************************************************/
/* LOCAL VARIABLES                                                            */
/******************************************************************************/

/**
 * @brief   Name of canbridge nodes.
 */
static const char _canbridge_name[] = "CANBridge";

/******************************************************************************/
/* LOCAL FUNCTIONS                                                            */
/******************************************************************************/

void getFrameTopicData(const CANRxFrame* frame, can_node_t* can) {
  id_map_t frame_map;
  frame_map.raw = frame->EID;

  switch(frame_map.id) {
    case CAN_FLOOR_PROX:
    case CAN_FLOOR_AMB:
    case CAN_GYRO: {
      if (can->dwd.data_read == &can->dwd.data[0]) {
        if (frame_map.cnt == 0) {
          can->dwd.data[1].id = frame_map.id;
          memcpy(&can->dwd.timestamp[1], frame->data8, sizeof(frame->data8));
        } else {
          memcpy(&can->dwd.data[1].data[(frame_map.cnt-1)*4], frame->data8, sizeof(frame->data8));
        }
      } else {
        if (frame_map.cnt == 0) {
          can->dwd.data[0].id = frame_map.id;
          memcpy(&can->dwd.timestamp[0], frame->data8, sizeof(frame->data8));
        } else {
          memcpy(&can->dwd.data[0].data[(frame_map.cnt-1)*4], frame->data8, sizeof(frame->data8));
        }
      }

      if (frame_map.cnt == 0) {
        // set the pointer to the data which should be published
        if (can->dwd.data_read == &can->dwd.data[0]) {
          can->dwd.data_read = &can->dwd.data[1];
          can->dwd.timestamp_read = &can->dwd.timestamp[1];
        } else {
          can->dwd.data_read = &can->dwd.data[0];
          can->dwd.timestamp_read = &can->dwd.timestamp[0];
        }

        //Broadcast CAN_FRAMEEVENT so the node is signaled
        chEvtBroadcastFlagsI(&((can_node_t*)can)->frame_data.source, (urt_osEventFlags_t)0);
      }
      break;
    }
  default: break;
  }
  return;
}

bool transmitSubscriberData(can_node_t* can) {
  urt_osTime_t time;
  // fetch latest motion message
  {
    can_data_t data;
    if (urtHrtSubscriberFetchLatestMessage(&can->subscriber,
                                           &data,
                                           NULL,
                                           &time,
                                           NULL) != URT_STATUS_OK) {
      return true;
    }
    can->filter_map.id = data.id;
    can->filter_map.type = CAN_TOPIC_DATA;
    can->filter_map.cnt = sizeof(data.data)/sizeof(((CANTxFrame*)0)->data8) +1; //One frame for the timestamp

    while (can->filter_map.cnt != 0) {
      can->filter_map.cnt--;
      // send data from the subscriber over the CAN Bus
      CANTxFrame frame = {
        {
          /* DLC */ sizeof(frame.data8),
          /* RTR */ CAN_RTR_DATA,
          /* IDE */ CAN_IDE_EXT,
        },
        /* ID */ {{}},
        /* data */ {},
      };
      frame.EID = can->filter_map.raw;

      // first frame holds the timestamp
      if (can->filter_map.cnt == 0) {
        memcpy(frame.data8, &time, sizeof(frame.data8));
      } else {
        memcpy(frame.data8, &data.data[(can->filter_map.cnt -1)*4], sizeof(frame.data8));
      }

      // send the frame
      aosFBCanTransmitTimeout(&MODULE_HAL_CAN, &frame, TIME_MAX_INTERVAL);
    }
  }

  return false;
}

static void _canCallback(const CANRxFrame* frame, void* can)
{
  id_map_t frame_map;
  frame_map.raw = frame->EID;

  switch (frame_map.type) {
  case CAN_TOPIC_DATA: {
    getFrameTopicData(frame, ((can_node_t*)can));
    break;
    }
    default:break;
    break;
  }

  return;
}
/**
 * @brief   Setup callback function for canbridge nodes.
 *
 * @param[in] node   Pointer to the node object.
 *                   Must not be NULL.
 * @param[in] can    Pointer to the canbridge structure.
 *                   Must nor be NULL.
 *
 * @return  Event mask to listen for next.
 */
urt_osEventMask_t _canbridge_Setup(urt_node_t* node, void* can)
{
  urtDebugAssert(can != NULL);
  (void)node;

#if CH_CFG_USE_REGISTRY == TRUE
  // set thread name
  chRegSetThreadName(_canbridge_name);
#endif

  // set callback of the can filter
  ((can_node_t*)can)->can_filter.callback = _canCallback;
  // add CAN Bus filter to the CAN driver
  aosFBCanAddFilter(&MODULE_HAL_CAN, &((can_node_t*)can)->can_filter);
  // Node should listen to Frame events (new frame should be published)
  ((can_node_t*)can)->frame_data.events = CAN_FRAMEEVENT;

  // subscribe to module topic
  if (((can_node_t*)can)->subscriber.topic != 0) {
    // Node should also listen to Data events (new data are coming in)
    ((can_node_t*)can)->frame_data.events |= CAN_DATAEVENT;
    // kind of a hack to save memory
    urt_topic_t* const topic = ((can_node_t*)can)->subscriber.topic;
    ((can_node_t*)can)->subscriber.topic = NULL;
    urtHrtSubscriberSubscribe(&((can_node_t*)can)->subscriber, topic, CAN_DATAEVENT,
                              0, 0, 0);
  }

  if ((((id_map_t)((uint32_t)((can_node_t*)can)->can_filter.filter.ext.id)).id & CAN_GYRO) == CAN_GYRO) {
    urtPublisherInit(&((can_node_t*)can)->dwd.gyro_pub,
                     urtCoreGetTopic(CAN_GYRO));
  }
  if ((((id_map_t)((uint32_t)((can_node_t*)can)->can_filter.filter.ext.id)).id & CAN_FLOOR_AMB) == CAN_FLOOR_AMB) {
    urtPublisherInit(&((can_node_t*)can)->dwd.amb_pub,
                     urtCoreGetTopic(CAN_FLOOR_AMB));
  }
  if ((((id_map_t)((uint32_t)((can_node_t*)can)->can_filter.filter.ext.id)).id & CAN_FLOOR_PROX) == CAN_FLOOR_PROX) {
    urtPublisherInit(&((can_node_t*)can)->dwd.prox_pub,
                     urtCoreGetTopic(CAN_FLOOR_PROX));
  }

  // register the queue event to the event listener
  urtEventRegister(&((can_node_t*)can)->frame_data.source, &((can_node_t*)can)->frame_data.listener, CAN_FRAMEEVENT,
                   0);

  return ((can_node_t*)can)->frame_data.events;
}

/**
 * @brief   Loop callback function for canbridge nodes.
 *
 * @param[in] node  Pointer to the node object.
 *                  Must not be NULL.
 * @param[in] can   Pointer to the canbridge structure.
 *                  Must not be NULL.
 *
 * @return  Event mask to listen for next.
 */
urt_osEventMask_t _canbridge_Loop(urt_node_t* node, urt_osEventMask_t event, void* can)
{
  urtDebugAssert(can != NULL);

  (void)node;

  if (event == CAN_DATAEVENT) {
    if (transmitSubscriberData(((can_node_t*)can))) {
      return ((can_node_t*)can)->frame_data.events;
    }
  }

  if (event == CAN_FRAMEEVENT) {
    const can_data_t canData = *((can_node_t*)can)->dwd.data_read;
    switch(canData.id) {
    case CAN_GYRO: {
      urtPublisherPublish(&((can_node_t*)can)->dwd.gyro_pub,
                          ((gyro_data_t*)&canData),
                          sizeof(gyro_data_t),
                          ((can_node_t*)can)->dwd.timestamp_read,
                          URT_PUBLISHER_PUBLISH_ENFORCING);
      break;
    }
    case CAN_FLOOR_AMB: {
      urtPublisherPublish(&((can_node_t*)can)->dwd.amb_pub,
                          ((floor_data_t*)&canData),
                          sizeof(floor_data_t),
                          ((can_node_t*)can)->dwd.timestamp_read,
                          URT_PUBLISHER_PUBLISH_ENFORCING);
      break;
    }
    case CAN_FLOOR_PROX: {
      urtPublisherPublish(&((can_node_t*)can)->dwd.prox_pub,
                          ((floor_data_t*)&canData),
                          sizeof(floor_data_t),
                          ((can_node_t*)can)->dwd.timestamp_read,
                          URT_PUBLISHER_PUBLISH_ENFORCING);
      break;
    }
    default: break;
    }
  }

  return ((can_node_t*)can)->frame_data.events;
}

/**
 * @brief   Shutdown callback function for canbridge nodes.
 *
 * @param[in] node  Pointer to the node object.
 *                  Must not be NULL.
 * @param[in] can   Pointer to the canbridge structure.
 *                  Must not be NULL.
 */
void _canbridge_Shutdown(urt_node_t* node, urt_status_t reason, void* can)
{
  urtDebugAssert(can != NULL);

  (void)node;
  (void)reason;

  // unsubscribe from module specific topic
  urtHrtSubscriberUnsubscribe(&((can_node_t*)can)->subscriber);

  return;
}

/******************************************************************************/
/* EXPORTED FUNCTIONS                                                         */
/******************************************************************************/

void canLogicInit(can_node_t* can, urt_topicid_t topicid, urt_osThreadPrio_t prio)
{
  urtDebugAssert(can != NULL);

  // initialize subscriber
  if (topicid != 0) {
    urtHrtSubscriberInit(&can->subscriber);
    can->subscriber.topic = urtCoreGetTopic(topicid); // kind of a hack to save memory
  }

  // initialize the node
  urtNodeInit(&can->node, (urt_osThread_t*)can->thread, sizeof(can->thread), prio,
              _canbridge_Setup, can,
              _canbridge_Loop, can,
              _canbridge_Shutdown, can);

  // set/initialize event data
  urtEventSourceInit(&can->frame_data.source);
  urtEventListenerInit(&can->frame_data.listener);

  return;
}
