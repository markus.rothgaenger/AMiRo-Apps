/*
AMiRo-Apps is a collection of applications for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2021  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CANBRIDGE_H
#define CANBRIDGE_H

#include <urt.h>

#include "../../messagetypes/DWD_floordata.h"
#include "../../messagetypes/DWD_accodata.h"
#include "../../messagetypes/DWD_compassdata.h"
#include "../../messagetypes/DWD_gyrodata.h"
#include "../../messagetypes/ProximitySensordata.h"
#include "../../messagetypes/LightRing_leddata.h"
#include "../../messagetypes/CAN_IDs.h"
#include "../../messagetypes/motiondata.h"

/******************************************************************************/
/* CONSTANTS                                                                  */
/******************************************************************************/

#if !defined(CAN_STACKSIZE) || defined(__DOXYGEN__)
/**
 * @brief   Stack size of can threads.
 */
#define CAN_STACKSIZE             512
#endif /* !defined(CAN_STACKSIZE) */

/**
 * @brief   Event mask to be set on subscriber events.
 */
#define CAN_DATAEVENT                           (urtCoreGetEventMask() << 1)

#define CAN_FRAMEEVENT                          (urtCoreGetEventMask() << 2)

/******************************************************************************/
/* SETTINGS                                                                   */
/******************************************************************************/

/******************************************************************************/
/* CHECKS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* DATA STRUCTURES AND TYPES                                                  */
/******************************************************************************/

typedef union id_map {
  uint32_t raw;
  struct {
    uint32_t cnt : 11;
    uint32_t id : 16;
    uint32_t type : 2;
  };
}id_map_t;


/**
 * @brief   dwd_data.
 * @struct  Data which are coming from the DiWheelDrive module.
 */
typedef struct dwd_data
{
  urt_publisher_t prox_pub;
  urt_publisher_t amb_pub;
  urt_publisher_t gyro_pub;

  can_data_t data[2];
  can_data_t* data_read;
  urt_osTime_t timestamp[2];
  urt_osTime_t* timestamp_read;
}dwd_data_t;


/**
 * @brief   canLogic node.
 * @struct  canLogic_node
 */
typedef struct can_node {
  /**
   * @brief   Thread memory.
   */
  URT_THREAD_MEMORY(thread, CAN_STACKSIZE);

  /**
   * @brief Filter IDs where the CAN Bus should listen/transmit
   */
  aos_fbcan_filter_t can_filter;

  /**
   * @brief frame ids
   */
  id_map_t filter_map;

  /**
   * @brief   Node object.
   */
  urt_node_t node;

  /**
   * @brief used to signal the node when the frame data are transmitted completely over the CAN Bus
   */
  struct {
    urt_osEventSource_t source;
    urt_osEventListener_t listener;
    urt_osEventMask_t events; //events where the node loop listens to
  }frame_data;

  /**
   * @brief   Subscriber to receive information from the own module.
   */
  urt_hrtsubscriber_t subscriber;

  /**
   * @brief   Data coming from the DiWheelDrive.
   */
  dwd_data_t dwd;

  int callback_test;
} can_node_t;

/******************************************************************************/
/* MACROS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* EXTERN DECLARATIONS                                                        */
/******************************************************************************/

#if defined(__cplusplus)
extern "C" {
#endif /* defined(__cplusplus) */
  void canLogicInit(can_node_t* can, urt_topicid_t topicid, urt_osThreadPrio_t prio);
#if defined(__cplusplus)
}
#endif /* defined(__cplusplus) */

/******************************************************************************/
/* INLINE FUNCTIONS                                                           */
/******************************************************************************/

/******************************************************************************/
/* SUBSYSTEMS                                                                 */
/******************************************************************************/

#endif /* FLOOR_H */

/** @} */
