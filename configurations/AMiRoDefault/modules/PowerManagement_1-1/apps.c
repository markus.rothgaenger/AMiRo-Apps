/*
AMiRo-Apps is a collection of applications for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2021  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    apps.c
 * @brief   AMiRoDefault configuration application container.
 */

#include "apps.h"
#include <canbridge.h>
#include <ring.h>
#include <amiroos.h>

/******************************************************************************/
/* LOCAL DEFINITIONS                                                          */
/******************************************************************************/

/******************************************************************************/
/* EXPORTED VARIABLES                                                         */
/******************************************************************************/

/******************************************************************************/
/* LOCAL TYPES                                                                */
/******************************************************************************/

/******************************************************************************/
/* LOCAL VARIABLES                                                            */
/******************************************************************************/

/*
 * Topics for the different data.
 */
static urt_topic_t _dwd_prox_topic;
static urt_topic_t _dwd_amb_topic;
static urt_topic_t _dwd_gyro_topic;

/*
 * Payloads of the different data.
 */
static floor_data_t _dwd_prox_payload;
static floor_data_t _dwd_amb_payload;
static gyro_data_t _dwd_gyro_payload;

/*
 * CAN node instance
 */
static can_node_t _can;

/*
 * Ring node instance
 */
static ring_node_t _ring;

/******************************************************************************/
/* LOCAL FUNCTIONS                                                            */
/******************************************************************************/

#if (AMIROOS_CFG_SHELL_ENABLE == true) || defined(__DOXYGEN__)

static int _CANShellCmdCb(BaseSequentialStream* stream, int argc, const char* argv[])
{
  urtPrintf("CAN Callback: %i \n", _can.callback_test);
  return NULL;
}

static AOS_SHELL_COMMAND(_CANShellCmd, "CAN:Callback", _CANShellCmdCb);

#endif /* (AMIROOS_CFG_SHELL_ENABLE == true) */

/******************************************************************************/
/* EXPORTED FUNCTIONS                                                         */
/******************************************************************************/

/**
 * @brief   Initializes all data applications for the AMiRo default configuration.
 */
void appsInit(void)
{
  // initialize the different topics
  urtTopicInit(&_dwd_amb_topic, CAN_FLOOR_AMB, &_dwd_amb_payload);
  urtTopicInit(&_dwd_prox_topic, CAN_FLOOR_PROX, &_dwd_prox_payload);
  urtTopicInit(&_dwd_gyro_topic, CAN_GYRO, &_dwd_gyro_payload);

  id_map_t ids;
  ids.cnt = 0;
  ids.id = ~(CAN_FLOOR_PROX ^ CAN_FLOOR_AMB ^ CAN_GYRO); //ID must match
  ids.type = ~0; //Type must match

  id_map_t filter_id;
  filter_id.cnt = 0;
  filter_id.id = CAN_FLOOR_PROX | CAN_FLOOR_AMB | CAN_GYRO; //CAN Bridge of the PWM should receive messages with this ID only
  filter_id.type = CAN_TOPIC_DATA;

  // specify CAN Bus filter to get only for this app relevant messages
  _can.can_filter.mask.ext.id = ids.raw; //Equal bit of these two masks must match other not
  _can.can_filter.mask.ext.ide = ~0;
  _can.can_filter.mask.ext.rtr = ~0;
  _can.can_filter.filter.ext.id = filter_id.raw; // should only receive messages from this ID
  _can.can_filter.filter.ext.ide = CAN_IDE_EXT;
  _can.can_filter.filter.ext.rtr = CAN_RTR_DATA;
  _can.can_filter.cbparams = &_can;

  // initialize CAN app
  canLogicInit(&_can,
               NULL,
               URT_THREAD_PRIO_NORMAL_MAX);

  ringInit(&_ring, NULL, NULL, URT_THREAD_PRIO_NORMAL_MIN);

#if (AMIROOS_CFG_SHELL_ENABLE == true) || defined(__DOXYGEN__)
  aosShellAddCommand(&_CANShellCmd);
#endif /* (AMIROOS_CFG_SHELL_ENABLE == true) || defined(__DOXYGEN__) */
  return;
}
