/*
AMiRo-Apps is a collection of applications for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2021  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    urtconf.h
 * @brief   µRT configuration file for the PowerManagement (v1.1) AMiRo module.
 * @details Contains the module specific µRT settings.
 *
 * @addtogroup AMiRoDefault_PowerManagement_1-1
 * @{
 */

#ifndef URTCONF_H
#define URTCONF_H

#include <AMiRoDefault_urtconf.h>

#endif /* URTCONF_H */

/** @} */
