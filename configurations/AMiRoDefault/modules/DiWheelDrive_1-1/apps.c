/*
AMiRo-Apps is a collection of applications for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2021  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    apps.c
 * @brief   AMiRoDefault configuration application container.
 */

#include "apps.h"
#include <math.h>
#include <amiroos.h>
#include <differentialmotionestimator.h>
#include <differentialmotorcontrol.h>
#include <floor.h>
#include <gyroscope.h>
#include <canbridge.h>
#include <accelerometer.h>
#include <compass.h>

/******************************************************************************/
/* LOCAL DEFINITIONS                                                          */
/******************************************************************************/

#if !defined(M_PI)
#define M_PI                                      3.14159265358979323846
#endif

/**
 * @brief   Wheel diameter.
 * @todo    Should be two values (left & right) stored at EEPROM, so that individual robots can have individual values.
 */
#define WHEEL_DIAMETER                            55710

/**
 * @brief   Wheel diameter.
 * @todo    Should be stored at EEPROM, so that individual robots can have individual values.
 */
#define WHEEL_OFFSET                              (34000 + (9000/2))

/**
 * @brief   Topic ID for DME motion information.
 */
#define DME_TOPIC_ID                              1

/**
 * @brief   Service ID for setting DMC target information.
 */
#define DMC_TARGETSERVICE_ID                      1

/**
 * @brief   Service ID for executing DMC auto calibration.
 */
#define DMC_CALIBSERVICE_ID                       2

/**
 * @brief   Frequency of the DME in Hertz.
 */
#define DME_FREQUENCY                             100

#define PROXIMITY_FLOOR_TOPICID       3 //Topic identifier for the proximity floor topic
#define AMBIENT_FLOOR_TOPICID         4 //Topic identifier for the ambient floor topic
#define GYRO_TOPICID                  5 //Topic identifier for the gyroscope topic
#define ACCO_TOPICID                  6 //Topic identifier for the accelerometer topic
#define COMPASS_TOPICID               7 //Topic identifier for the compass topic
#define CAN_TOPICID                   8 //Topic identifier for data which should be send over the CANBus

/******************************************************************************/
/* EXPORTED VARIABLES                                                         */
/******************************************************************************/

/******************************************************************************/
/* LOCAL TYPES                                                                */
/******************************************************************************/

/******************************************************************************/
/* LOCAL VARIABLES                                                            */
/******************************************************************************/

/*
 * Topics for the different data.
 */
static urt_topic_t _proximity_floor_topic;
static urt_topic_t _ambient_floor_topic;
static urt_topic_t _gyro_topic;
static urt_topic_t _acco_topic;
static urt_topic_t _compass_topic;
static urt_topic_t _can_topic;

/*
 * Payloads of the different data.
 */
static floor_sensors_t _proximity_floor_payload;
static floor_sensors_t _ambient_floor_payload;
static gyro_sensor_t   _gyro_payload;
static acco_sensor_t   _acco_payload;
static compass_sensor_t _compass_payload;
static floor_data_t _can_payload;

/**
 * @brief   DME instance.
 */
static dme_t _dme;

/*
 * Floor node instance.
 */
static floor_node_t _floor;

/*
 * Gyroscope node instance
 */
static gyro_node_t _gyro;

/*
 * Accelerometer node instance
 */
static acco_node_t _acco;

/*
 * Compass node instance
 */
static compass_node_t _compass;

/*
 * CAN node instance
 */
static can_node_t _can;

/*
 * Driver frequency (in Hz) of the floor sensors, gyroscope and accelerometer.
 */
const float frequency = 50;

/**
 * @brief   DME configuration.
 */
static const dme_config_t _dme_config = {
  .left = {
    .circumference = (M_PI * WHEEL_DIAMETER) + 0.5f,
    .offset = WHEEL_OFFSET,
    .qei = {
      .driver = &MODULE_HAL_QEI_LEFT_WHEEL,
      .increments_per_revolution = MODULE_HAL_QEI_INCREMENTS_PER_REVOLUTION,
    },
  },
  .right = {
    .circumference = (M_PI * WHEEL_DIAMETER) + 0.5f,
    .offset = WHEEL_OFFSET,
    .qei = {
      .driver = &MODULE_HAL_QEI_RIGHT_WHEEL,
      .increments_per_revolution = MODULE_HAL_QEI_INCREMENTS_PER_REVOLUTION,
    },
  },
  .interval = MICROSECONDS_PER_SECOND / DME_FREQUENCY,
};

/**
 * @brief   DME motion topic.
 */
static urt_topic_t _dme_motion_topic;

/**
 * @brief   Payload for the DME motion topic mandatory message.
 */
static dme_motionpayload_t _dme_motion_topic_payload;

/**
 * @brief   DMC instance.
 */
static dmc_t _dmc;

/**
 * @brief   DMC configuration.
 */
static const dmc_config_t _dmc_config = {
  .motors = {
    .left = {
      .forward = {
        .driver = &MODULE_HAL_PWM_DRIVE,
        .channel = MODULE_HAL_PWM_DRIVE_CHANNEL_LEFT_FORWARD,
      },
      .reverse= {
        .driver = &MODULE_HAL_PWM_DRIVE,
        .channel = MODULE_HAL_PWM_DRIVE_CHANNEL_LEFT_BACKWARD,
      },
    },
    .right = {
      .forward = {
        .driver = &MODULE_HAL_PWM_DRIVE,
        .channel = MODULE_HAL_PWM_DRIVE_CHANNEL_RIGHT_FORWARD,
      },
      .reverse= {
        .driver = &MODULE_HAL_PWM_DRIVE,
        .channel = MODULE_HAL_PWM_DRIVE_CHANNEL_RIGHT_BACKWARD,
      },
    },
  },
  .lpf = {
    .factor = 10.0f,
    .max = {
      .steering = 3.14f,
      .left = 1.0f,
      .right = 1.0f,
    },
  },
};

/******************************************************************************/
/* LOCAL FUNCTIONS                                                            */
/******************************************************************************/

#if (AMIROOS_CFG_SHELL_ENABLE == true) || defined(__DOXYGEN__)

/**
 * @brief   DMC target velocity shell command callback.
 *
 * @param[in] stream  Shell I/O stream.
 * @param[in] argc    Number of command arguments.
 * @param[in] argv    Argument list.
 *
 * @return  The operation result.
 */
static int _appsDmcShellCmdCb_setVelocity(BaseSequentialStream* stream, int argc, const char* argv[])
{
  return dmcShellCallback_setVelocity(stream, argc, argv, urtCoreGetService(DMC_TARGETSERVICE_ID));
}

/**
 * @brief   DMC target velocity shell command.
 */
static AOS_SHELL_COMMAND(_appsDmcShellCmd_setVelocity, "DMC:setVelocity", _appsDmcShellCmdCb_setVelocity);

/**
 * @brief   DMC get gains shell command callback.
 *
 * @param[in] stream  Shell I/O stream.
 * @param[in] argc    Number of arguments.
 * @param[in] argv    Argument list.
 *
 * @return  The operation result.
 */
static int _appsDmcShellCmdCb_getGains(BaseSequentialStream* stream, int argc, const char* argv[])
{
  urtPrintf("floor amb 0: %u \n", _floor.driver_data.amb_data.values.data[0]);
  urtPrintf("can: %i \n", _can.callback_test);
  return 0;
  return dmcShellCallback_getGains(stream, argc, argv, &_dmc);
}

/**
 * @brief   DMC get gains shell command.
 */
static AOS_SHELL_COMMAND(_appsDmcShellCmd_getGains, "DMC:getGains", _appsDmcShellCmdCb_getGains);

/**
 * @brief   DMC set gains shell command callback.
 *
 * @param[in] stream  Shell I/O stream.
 * @param[in] argc    Number of arguments.
 * @param[in] argv    Argument list.
 *
 * @return  The operation result.
 */
static int _appsDmcShellCmdCb_setGains(BaseSequentialStream* stream, int argc, const char* argv[])
{
  return dmcShellCallback_setGains(stream, argc, argv, &_dmc);
}

/**
 * @brief   DMC set gains shell command.
 */
static AOS_SHELL_COMMAND(_appsDmcShellCmd_setGains, "DMC:setGains", _appsDmcShellCmdCb_setGains);

#if (DMC_CALIBRATION_ENABLE == true) || defined(__DOXYGEN__)

/**
 * @brief   DMC auto calibration shell coammand callback.
 *
 * @param[in] stream  Shell I/O stream.
 * @param[in] argc    Number of arguments.
 * @param[in] argv    Argument list.
 *
 * @return  The operation result.
 */
static int _appsDmcShellCmdCb_autoCalibration(BaseSequentialStream* stream, int argc, const char* argv[])
{
  return dmcShellCallback_autoCalibration(stream, argc, argv, urtCoreGetService(DMC_CALIBSERVICE_ID));
}

/**
 * @brief   DMC auto calibration shell coammand.
 */
static AOS_SHELL_COMMAND(_appsDmcShellCmd_autoCalibration, "DMC:calibration", _appsDmcShellCmdCb_autoCalibration);

#endif /* (DMC_CALIBRATION_ENABLE == true) */

#endif /* (AMIROOS_CFG_SHELL_ENABLE == true) || defined(__DOXYGEN__) */

/******************************************************************************/
/* EXPORTED FUNCTIONS                                                         */
/******************************************************************************/

/**
 * @brief   Initializes all data applications for the AMiRo default configuration.
 */
void appsInit(void)
{
  // initialize the different topics
  urtTopicInit(&_dme_motion_topic, DME_TOPIC_ID, &_dme_motion_topic_payload);
  urtTopicInit(&_proximity_floor_topic, PROXIMITY_FLOOR_TOPICID, &_proximity_floor_payload);
  urtTopicInit(&_ambient_floor_topic, AMBIENT_FLOOR_TOPICID, &_ambient_floor_payload);
  urtTopicInit(&_gyro_topic, GYRO_TOPICID, &_gyro_payload);
  urtTopicInit(&_acco_topic, ACCO_TOPICID, &_acco_payload);
  urtTopicInit(&_compass_topic, COMPASS_TOPICID, &_compass_payload);
  urtTopicInit(&_can_topic, CAN_TOPICID, &_can_payload);

  // initialize DME app
  dmeInit(&_dme, &_dme_config, DME_TOPIC_ID, URT_THREAD_PRIO_RT_MAX);

  // initialize DMC app
#if (DMC_CALIBRATION_ENABLE == true)
  dmcInit(&_dmc, &_dmc_config, DME_TOPIC_ID, DMC_TARGETSERVICE_ID, DMC_CALIBSERVICE_ID, URT_THREAD_PRIO_RT_MAX);
#else
  dmcInit(&_dmc, &_dmc_config, DME_TOPIC_ID, DMC_TARGETSERVICE_ID, URT_THREAD_PRIO_RT_MAX);
#endif

  // initialize Floor app
  floorInit(&_floor,
            AMBIENT_FLOOR_TOPICID,
            PROXIMITY_FLOOR_TOPICID,
            CAN_TOPICID,
            URT_THREAD_PRIO_NORMAL_MIN,
            frequency);

  // initialize Gyroscope app
  gyroInit(&_gyro,
           GYRO_TOPICID,
           CAN_TOPICID,
           URT_THREAD_PRIO_NORMAL_MIN,
           frequency);

  // initialize Accelerometer app
  accoInit(&_acco,
           ACCO_TOPICID,
           URT_THREAD_PRIO_NORMAL_MIN,
           frequency);

  // initialize Compass app
  compassInit(&_compass,
           COMPASS_TOPICID,
           URT_THREAD_PRIO_NORMAL_MIN,
           frequency);

  // initialize CAN app
  canLogicInit(&_can,
               CAN_TOPICID,
               URT_THREAD_PRIO_NORMAL_MAX);

#if (AMIROOS_CFG_SHELL_ENABLE == true) || defined(__DOXYGEN__)
  // add DMC shell commands
  aosShellAddCommand(&_appsDmcShellCmd_setVelocity);
  aosShellAddCommand(&_appsDmcShellCmd_getGains);
  aosShellAddCommand(&_appsDmcShellCmd_setGains);
#if (DMC_CALIBRATION_ENABLE == true)
  aosShellAddCommand(&_appsDmcShellCmd_autoCalibration);
#endif /* (DMC_CALIBRATION_ENABLE == true) */
#endif /* (AMIROOS_CFG_SHELL_ENABLE == true) || defined(__DOXYGEN__) */

  return;
}
