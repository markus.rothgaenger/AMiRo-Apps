"""
AMiRo-Apps is a collection of applications for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2021  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import sys
import readline
import glob
import os
import re
import numpy as np

#------------------------------------------------------------------------------
# local functions
#------------------------------------------------------------------------------

def complete(text, state):
    return (glob.glob(text+'*')+[None])[state]

def createOutputFolder(path):
    if len(os.path.dirname(path)):
        try:
            os.mkdir(path)
        except FileExistsError:
            print("ERROR: Folder " + path + " already exists.")
            quit()
        print("Created output folder: " + path)
    else:
        print("WARNING: No output folder defined.")
        quit()

#------------------------------------------------------------------------------
# script
#------------------------------------------------------------------------------

# configure auto completion for user input
readline.set_completer_delims(' \t\n;')
readline.parse_and_bind("tab: complete")
readline.set_completer(complete)

# input file
if len(sys.argv) > 1:
    inpath = sys.argv[1]
else:
    inpath = input("path to input file: ")
try:
    infile = open(inpath, 'r')
except IOError:
    print("ERROR: Unable to open input file.")
    quit()
infile_dir = os.path.dirname(inpath)
infile_name = os.path.basename(inpath)
infile_base = os.path.splitext(infile_name)[0]

# output path and files
if len(sys.argv) > 2:
    outpath = sys.argv[2]
else:
    outpath = input("path to create output file folder: ")
outpath = os.path.join(outpath, infile_base)
outdircreated = False
outfiles_ext = '.tsv'
outfiles_bases = {'nodes':               'nodes',
                  'pubsubpayloadnrt':    'pubsub_payload_nrt',
                  'pubsubpayloadsrt':    'pubsub_payload_srt',
                  'pubsubpayloadfrt':    'pubsub_payload_frt',
                  'pubsubpayloadhrt':    'pubsub_payload_hrt',
                  'pubsubmessagesnrt':   'pubsub_messagebuffer_nrt',
                  'pubsubmessagessrt':   'pubsub_messagebuffer_srt',
                  'pubsubmessagesfrt':   'pubsub_messagebuffer_frt',
                  'pubsubmessageshrt':   'pubsub_messagebuffer_hrt',
                  'pubsubpublishernrt':  'pubsub_publisher_nrt',
                  'pubsubpublishersrt':  'pubsub_publisher_srt',
                  'pubsubpublisherfrt':  'pubsub_publisher_frt',
                  'pubsubpublisherhrt':  'pubsub_publisher_hrt',
                  'pubsubsubscribernrt': 'pubsub_subscriber_nrt',
                  'pubsubsubscribersrt': 'pubsub_subscriber_srt',
                  'pubsubsubscriberfrt': 'pubsub_subscriber_frt',
                  'pubsubsubscriberhrt': 'pubsub_subscriber_hrt',
                  'pubsubtopicnrt':      'pubsub_topic_nrt',
                  'pubsubtopicsrt':      'pubsub_topic_srt',
                  'pubsubtopicfrt':      'pubsub_topic_frt',
                  'pubsubtopichrt':      'pubsub_topic_hrt',
                  'pubsubqosdeadline':   'pubsub_qos_deadline',
                  'pubsubqosjitter':     'pubsub_qos_jitter',
                  'pubsubqosrate':       'pubsub_qos_rate',
                  'rpcpayloadnrt':       'rpc_payload_nrt',
                  'rpcpayloadsrt':       'rpc_payload_srt',
                  'rpcpayloadfrt':       'rpc_payload_frt',
                  'rpcpayloadhrt':       'rpc_payload_hrt',
                  'rpcrequestnrt':       'rpc_request_nrt',
                  'rpcrequestsrt':       'rpc_request_srt',
                  'rpcrequestfrt':       'rpc_request_frt',
                  'rpcrequesthrt':       'rpc_request_hrt',
                  'rpcservicenrt':       'rpc_service_nrt',
                  'rpcservicesrt':       'rpc_service_srt',
                  'rpcservicefrt':       'rpc_service_frt',
                  'rpcservicehrt':       'rpc_service_hrt',
                  'rpcqosdeadline':      'rpc_qos_deadline',
                  'rpcqosjitter':        'rpc_qos_jitter'}

# FSM to parse input file
state = 'searchprompt'
for line in infile:

    """
    Detect and identify headers first.
    """

    # search for first shell prompt
    if state == 'searchprompt':
       if re.match(r'[a-zA-Z0-9\[\] _-]*\$ ', line):
           state = 'search'

    # search for publish-subscribe data
    elif state == 'searchpubsub':
        if re.match(r'\-+ PAYLOAD \(NRT\) \-+', line):
            print("parsing publish-subscribe payload (NRT)...", end='', flush=True)
            state = 'urtpubsubpayloadnrt'
            continue
        elif re.match(r'\-+ PAYLOAD \(SRT\) \-+', line):
            print("parsing publish-subscribe payload (SRT)...", end='', flush=True)
            state = 'urtpubsubpayloadsrt'
            continue
        elif re.match(r'\-+ PAYLOAD \(FRT\) \-+', line):
            print("parsing publish-subscribe payload (FRT)...", end='', flush=True)
            state = 'urtpubsubpayloadfrt'
            continue
        elif re.match(r'\-+ PAYLOAD \(HRT\) \-+', line):
            print("parsing publish-subscribe payload (HRT)...", end='', flush=True)
            state = 'urtpubsubpayloadhrt'
            continue
        elif re.match(r'\-+ MESSAGEBUFFER \(NRT\) \-+', line):
            print("parsing publish-subscribe message buffer (NRT)...", end='', flush=True)
            state = 'urtpubsubmessagesnrt'
            continue
        elif re.match(r'\-+ MESSAGEBUFFER \(SRT\) \-+', line):
            print("parsing publish-subscribe message buffer (SRT)...", end='', flush=True)
            state = 'urtpubsubmessagessrt'
            continue
        elif re.match(r'\-+ MESSAGEBUFFER \(FRT\) \-+', line):
            print("parsing publish-subscribe message buffer (FRT)...", end='', flush=True)
            state = 'urtpubsubmessagesfrt'
            continue
        elif re.match(r'\-+ MESSAGEBUFFER \(HRT\) \-+', line):
            print("parsing publish-subscribe message buffer (HRT)...", end='', flush=True)
            state = 'urtpubsubmessageshrt'
            continue
        elif re.match(r'\-+ PUBLISHER \(NRT\) \-+', line):
            print("parsing publish-subscribe publisher (NRT)...", end='', flush=True)
            state = 'urtpubsubpublishernrt'
            continue
        elif re.match(r'\-+ PUBLISHER \(SRT\) \-+', line):
            print("parsing publish-subscribe publisher (SRT)...", end='', flush=True)
            state = 'urtpubsubpublishersrt'
            continue
        elif re.match(r'\-+ PUBLISHER \(FRT\) \-+', line):
            print("parsing publish-subscribe publisher (FRT)...", end='', flush=True)
            state = 'urtpubsubpublisherfrt'
            continue
        elif re.match(r'\-+ PUBLISHER \(HRT\) \-+', line):
            print("parsing publish-subscribe publisher (HRT)...", end='', flush=True)
            state = 'urtpubsubpublisherhrt'
            continue
        elif re.match(r'\-+ SUBSCRIBER \(NRT\) \-+', line):
            print("parsing publish-subscribe subscriber (NRT)...", end='', flush=True)
            state = 'urtpubsubsubscribernrt'
            continue
        elif re.match(r'\-+ SUBSCRIBER \(SRT\) \-+', line):
            print("parsing publish-subscribe subscriber (SRT)...", end='', flush=True)
            state = 'urtpubsubsubscribersrt'
            continue
        elif re.match(r'\-+ SUBSCRIBER \(FRT\) \-+', line):
            print("parsing publish-subscribe subscriber (FRT)...", end='', flush=True)
            state = 'urtpubsubsubscriberfrt'
            continue
        elif re.match(r'\-+ SUBSCRIBER \(HRT\) \-+', line):
            print("parsing publish-subscribe subscriber (HRT)...", end='', flush=True)
            state = 'urtpubsubsubscriberhrt'
            continue
        elif re.match(r'\-+ TOPIC \(NRT\) \-+', line):
            print("parsing publish-subscribe topic (NRT)...", end='', flush=True)
            state = 'urtpubsubtopicnrt'
            continue
        elif re.match(r'\-+ TOPIC \(SRT\) \-+', line):
            print("parsing publish-subscribe topic (SRT)...", end='', flush=True)
            state = 'urtpubsubtopicsrt'
            continue
        elif re.match(r'\-+ TOPIC \(FRT\) \-+', line):
            print("parsing publish-subscribe topic (FRT)...", end='', flush=True)
            state = 'urtpubsubtopicfrt'
            continue
        elif re.match(r'\-+ TOPIC \(HRT\) \-+', line):
            print("parsing publish-subscribe topic (HRT)...", end='', flush=True)
            state = 'urtpubsubtopichrt'
            continue
        elif re.match(r'\-+ DEADLINE \-+', line):
            print("parsing publish-subscribe deadline...", end='', flush=True)
            state = 'urtpubsubqosdeadline'
            continue
        elif re.match(r'\-+ JITTER \-+', line):
            print("parsing publish-subscribe jitter...", end='', flush=True)
            state = 'urtpubsubqosjitter'
            continue
        elif re.match(r'\-+ RATE \-+', line):
            print("parsing publish-subscribe rate...", end='', flush=True)
            state = 'urtpubsubqosrate'
            continue
        elif re.match('#+ RPC #+', line):
            state = 'searchrpc'
            continue
        elif re.match('#+ [a-zA-Z0-9 -_] #+', line):
            state = 'search'
            continue
    
    # search for RPC data
    elif state == 'searchrpc':
        if re.match(r'\-+ PAYLOAD \(NRT\) \-+', line):
            print("parsing RPC payload (NRT)...", end='', flush=True)
            state = 'urtrpcpayloadnrt'
            continue
        elif re.match(r'\-+ PAYLOAD \(SRT\) \-+', line):
            print("parsing RPC payload (SRT)...", end='', flush=True)
            state = 'urtrpcpayloadsrt'
            continue
        elif re.match(r'\-+ PAYLOAD \(FRT\) \-+', line):
            print("parsing RPC payload (FRT)...", end='', flush=True)
            state = 'urtrpcpayloadfrt'
            continue
        elif re.match(r'\-+ PAYLOAD \(HRT\) \-+', line):
            print("parsing RPC payload (HRT)...", end='', flush=True)
            state = 'urtrpcpayloadhrt'
            continue
        elif re.match(r'\-+ REQUEST \(NRT\) \-+', line):
            print("parsing RPC request (NRT)...", end='', flush=True)
            state = 'urtrpcrequestnrt'
            continue
        elif re.match(r'\-+ REQUEST \(SRT\) \-+', line):
            print("parsing RPC request (SRT)...", end='', flush=True)
            state = 'urtrpcrequestsrt'
            continue
        elif re.match(r'\-+ REQUEST \(FRT\) \-+', line):
            print("parsing RPC request (FRT)...", end='', flush=True)
            state = 'urtrpcrequestfrt'
            continue
        elif re.match(r'\-+ REQUEST \(HRT\) \-+', line):
            print("parsing RPC request (HRT)...", end='', flush=True)
            state = 'urtrpcrequesthrt'
            continue
        elif re.match(r'\-+ SERVICE \(NRT\) \-+', line):
            print("parsing RPC service (NRT)...", end='', flush=True)
            state = 'urtrpcservicenrt'
            continue
        elif re.match(r'\-+ SERVICE \(SRT\) \-+', line):
            print("parsing RPC service (SRT)...", end='', flush=True)
            state = 'urtrpcservicesrt'
            continue
        elif re.match(r'\-+ SERVICE \(FRT\) \-+', line):
            print("parsing RPC service (FRT)...", end='', flush=True)
            state = 'urtrpcservicefrt'
            continue
        elif re.match(r'\-+ SERVICE \(HRT\) \-+', line):
            print("parsing RPC service (HRT)...", end='', flush=True)
            state = 'urtrpcservicehrt'
            continue
        elif re.match(r'\-+ DEADLINE \-+', line):
            print("parsing RPC deadline...", end='', flush=True)
            state = 'urtrpcqosdeadline'
            continue
        elif re.match(r'\-+ JITTER \-+', line):
            print("parsing RPC jitter...", end='', flush=True)
            state = 'urtrpcqosjitter'
            continue
        elif re.match('#+ PUBLISH-SUBSCRIBE #+', line):
            state = 'searchpubsub'
            continue
        elif re.match('#+ [a-zA-Z0-9 -_] #+', line):
            state = 'search'
            continue

    # search for general information (neither publish-subscribe nor RPC)
    elif state == 'search':
        if re.match('core frequency', line):
            state = 'mcufreq'
        elif re.search('Mass reschedule performance', line):
            state = 'chthreadperf'
            continue
        elif re.search('Virtual Timers set/reset performance', line):
            state = 'chtimerperf'
            continue
        elif re.search('Mutexes lock/unlock performance', line):
            state = 'chmutexperf'
            continue
        elif re.search('RAM Footprint', line):
            state = 'chram'
            continue
        elif re.match('maximum expected jitter', line):
            state = 'aosmaxjitter'
        elif re.match('#+ MEMORY #+', line):
            state = 'urtmemory'
            continue
        elif re.match('#+ NODE #+', line):
            if outdircreated == False:
                createOutputFolder(outpath)
                outdircreated = True
            print("parsing node...", end='', flush=True)
            state = 'urtnodes'
            continue
        elif re.match(r'#+ PUBLISH\-SUBSCRIBE #+', line):
            if outdircreated == False:
                createOutputFolder(outpath)
                outdircreated = True
            state = 'searchpubsub'
            continue
        elif re.match('#+ RPC #+', line):
            if outdircreated == False:
                createOutputFolder(outpath)
                outdircreated = True
            state = 'searchrpc'
            continue

    """
    Parse depending on detected type of information.
    """

    # MCU frequency
    if state == 'mcufreq':
        print("MCU\tfrequency\t" + re.search(r'\d+', line).group() + " MHz")
        state = 'search'

    # ChibiOS thread performance
    elif state == 'chthreadperf':
        print("ChibiOS\treschedule:\t" + str(1000000 / int(re.findall(r' \d+ ', line)[0])) + " us")
        print("ChibiOS\tcontext switch:\t" + str(1000000 / int(re.findall(r' \d+ ', line)[1])) + " us")
        state = 'search'

    # ChibiOS timer performance
    elif state == 'chtimerperf':
        print("ChibiOS\ttimer:\t" + str(1000000 / int(re.search(r' \d+ ', line).group())) + " us")
        state = 'search'

    # ChibiOS mutex performance
    elif state == 'chmutexperf':
        print("ChibiOS\tmutex:\t" + str(1000000 / int(re.search(r' \d+ ', line).group())) + " us")
        state = 'search'

    # ChibiOS memory footprint
    elif state == 'chram':
        if re.search(r'\d+ bytes$', line):
            print("ChibiOS\t" + re.search(r'\s[a-zA-Z.]+\s*:', line).group()[1:-1] + "\t" + re.search(r'\d+', line).group() + " bytes")
        else:
            state = 'search'

    # AMiRo-OS maximum expected jitter
    elif state == 'aosmaxjitter':
        print("AMiRo-OS\tmaximum expected jitter\t" + re.search(r'\d+ us', line).group()[:-3] + " us\t" + re.search(r'\d+ clock cycles', line).group()[:-13] + " clock cycles")
        state = 'search'

    # µRT memory footprint
    elif state == 'urtmemory':
        if len(line.strip()):
            print(line, end='')
        else:
            state = 'search'

    # µRT nodes benchmark
    elif state == 'urtnodes':
        if re.match('NODES\tNODE\tITERATION\tLATENCY', line):
            nodes = 1
            node = 1
            latencies = np.empty(shape=0, dtype=np.int32)
            evals = {nodes: {node: {}}}
        elif re.match(r'\s*\d+\s+\d+\s+\d+\s+\d+', line):
            values = np.array(re.findall(r'\d+', line), dtype=np.int32)
            if values[0] == nodes and values[1] == node:
                latencies = np.append(latencies, values[3])
            else:
                evals[nodes][node] = {'min': np.amin(latencies),
                                      'max': np.amax(latencies),
                                      'mean': np.mean(latencies),
                                      'median': np.median(latencies),
                                      'jitter': np.amax(latencies) - np.amin(latencies)}
                if values[0] != nodes:
                    evals[values[0]] = {values[1]: {}}
                nodes = values[0]
                node = values[1]
                latencies = np.array(values[3], dtype=np.int32)
        elif len(line.strip()) == 0:
            evals[nodes][node] = {'min': np.amin(latencies),
                                  'max': np.amax(latencies),
                                  'mean': np.mean(latencies),
                                  'median': np.median(latencies),
                                  'jitter': np.amax(latencies) - np.amin(latencies)}
            header = "nodes\tnode\tmin\tmax\tmean\tmedian\tjitter\n"
            for nodes, node_evals in evals.items():
                outfile = open(os.path.join(outpath, outfiles_bases['nodes'] + '_' + str(nodes).zfill(4) + outfiles_ext), 'w')
                outfile.write(header)
                for node, values in node_evals.items():
                    outfile.write(str(nodes) + "\t" + str(node) + "\t" + 
                                  str(values['min']) + "\t" + str(values['max']) + "\t" +
                                  str(values['mean']) + "\t" + str(values['median']) + "\t" +
                                  str(values['jitter']) + "\n")
                outfile.close()
            outfile = open(os.path.join(outpath, outfiles_bases['nodes'] + outfiles_ext), 'w')
            outfile.write(header)
            for nodes, node_evals in evals.items():
                worst = 0
                worstval = 0
                for node, values in node_evals.items():
                    if values['mean'] > worstval:
                        worst = node
                        worstval = values['mean']
                values = node_evals[worst]
                outfile.write(str(nodes) + "\t" + str(worst) + "\t" +
                              str(values['min']) + "\t" + str(values['max']) + "\t" +
                              str(values['mean']) + "\t" + str(values['median']) + "\t" +
                              str(values['jitter']) + "\n")
            outfile.close()
            print("done")
            state = 'search'

    # µRT publish-subscribe payload benchmarks
    elif state == 'urtpubsubpayloadnrt' or state == 'urtpubsubpayloadsrt' or state == 'urtpubsubpayloadfrt' or state == 'urtpubsubpayloadhrt':
        if re.match('PAYLOAD\tITERATION\tLATENCY', line):
            payload = 0
            latencies = np.empty(shape=0, dtype=np.int32)
            evals = {payload: {}}
        elif re.match(r'\s*\d+\s+\d+\s+\d+', line):
            values = np.array(re.findall(r'\d+', line), dtype=np.int32)
            if values[0] == payload:
                latencies = np.append(latencies, values[2])
            else:
                evals[payload] = {'min': np.amin(latencies),
                                  'max': np.amax(latencies),
                                  'mean': np.mean(latencies),
                                  'median': np.median(latencies),
                                  'jitter': np.amax(latencies) - np.amin(latencies)}
                payload = values[0]
                latencies = np.array(values[2], dtype=np.int32)
        elif len(line.strip()) == 0:
            evals[payload] = {'min': np.amin(latencies),
                              'max': np.amax(latencies),
                              'mean': np.mean(latencies),
                              'median': np.median(latencies),
                              'jitter': np.amax(latencies) - np.amin(latencies)}
            header = "payload\tmin\tmax\tmean\tmedian\tjitter\n"
            if state == 'urtpubsubpayloadnrt':
                outfile = open(os.path.join(outpath, outfiles_bases['pubsubpayloadnrt'] + outfiles_ext), 'w')
            elif state == 'urtpubsubpayloadsrt':
                outfile = open(os.path.join(outpath, outfiles_bases['pubsubpayloadsrt'] + outfiles_ext), 'w')
            elif state == 'urtpubsubpayloadfrt':
                outfile = open(os.path.join(outpath, outfiles_bases['pubsubpayloadfrt'] + outfiles_ext), 'w')
            elif state == 'urtpubsubpayloadhrt':
                outfile = open(os.path.join(outpath, outfiles_bases['pubsubpayloadhrt'] + outfiles_ext), 'w')
            outfile.write(header)
            for payload, values in evals.items():
                outfile.write(str(payload) + "\t" +
                              str(values['min']) + "\t" + str(values['max']) + "\t" +
                              str(values['mean']) + "\t" + str(values['median']) + "\t" +
                              str(values['jitter']) + "\n")
            outfile.close()
            print("done")
            state = 'searchpubsub'
    
    # µRT publish-subscribe messages benchmarks
    elif state == 'urtpubsubmessagesnrt' or state == 'urtpubsubmessagessrt' or state == 'urtpubsubmessagesfrt' or state == 'urtpubsubmessageshrt':
        if re.match('MESSAGES\tITERATION\tLATENCY', line):
            messages = 1
            latencies = np.empty(shape=0, dtype=np.int32)
            evals = {messages: {}}
        elif re.match(r'\s*\d+\s+\d+\s+\d+', line):
            values = np.array(re.findall(r'\d+', line), dtype=np.int32)
            if values[0] == messages:
                latencies = np.append(latencies, values[2])
            else:
                evals[messages] = {'min': np.amin(latencies),
                                   'max': np.amax(latencies),
                                   'mean': np.mean(latencies),
                                   'median': np.median(latencies),
                                   'jitter': np.amax(latencies) - np.amin(latencies)}
                messages = values[0]
                latencies = np.array(values[2], dtype=np.int32)
        elif len(line.strip()) == 0:
            evals[messages] = {'min': np.amin(latencies),
                               'max': np.amax(latencies),
                               'mean': np.mean(latencies),
                               'median': np.median(latencies),
                               'jitter': np.amax(latencies) - np.amin(latencies)}
            header = "messages\tmin\tmax\tmean\tmedian\tjitter\n"
            if state == 'urtpubsubmessagesnrt':
                outfile = open(os.path.join(outpath, outfiles_bases['pubsubmessagesnrt'] + outfiles_ext), 'w')
            elif state == 'urtpubsubmessagessrt':
                outfile = open(os.path.join(outpath, outfiles_bases['pubsubmessagessrt'] + outfiles_ext), 'w')
            elif state == 'urtpubsubmessagesfrt':
                outfile = open(os.path.join(outpath, outfiles_bases['pubsubmessagesfrt'] + outfiles_ext), 'w')
            elif state == 'urtpubsubmessageshrt':
                outfile = open(os.path.join(outpath, outfiles_bases['pubsubmessageshrt'] + outfiles_ext), 'w')
            outfile.write(header)
            for messages, values in evals.items():
                outfile.write(str(messages) + "\t" +
                              str(values['min']) + "\t" + str(values['max']) + "\t" +
                              str(values['mean']) + "\t" + str(values['median']) + "\t" +
                              str(values['jitter']) + "\n")
            outfile.close()
            print("done")
            state = 'searchpubsub'
    
    # µRT publish-subscribe publisher benchmark
    elif state == 'urtpubsubpublishernrt' or state == 'urtpubsubpublishersrt' or state == 'urtpubsubpublisherfrt' or state == 'urtpubsubpublisherhrt':
        if re.match('PUBLISHERS\tPUBLISHER\tITERATION\tLATENCY', line):
            publishers = 1
            publisher = 1
            latencies = np.empty(shape=0, dtype=np.int32)
            evals = {publishers: {publisher: {}}}
        elif re.match(r'\s*\d+\s+\d+\s+\d+\s+\d+', line):
            values = np.array(re.findall(r'\d+', line), dtype=np.int32)
            if values[0] == publishers and values[1] == publisher:
                latencies = np.append(latencies, values[3])
            else:
                evals[publishers][publisher] = {'min': np.amin(latencies),
                                                'max': np.amax(latencies),
                                                'mean': np.mean(latencies),
                                                'median': np.median(latencies),
                                                'jitter': np.amax(latencies) - np.amin(latencies)}
                if values[0] != publishers:
                    evals[values[0]] = {values[1]: {}}
                publishers = values[0]
                publisher = values[1]
                latencies = np.array(values[3], dtype=np.int32)
        elif len(line.strip()) == 0:
            evals[publishers][publisher] = {'min': np.amin(latencies),
                                            'max': np.amax(latencies),
                                            'mean': np.mean(latencies),
                                            'median': np.median(latencies),
                                            'jitter': np.amax(latencies) - np.amin(latencies)}
            header = "publishers\tpublisher\tmin\tmax\tmean\tmedian\tjitter\n"
            for publishers, publisher_evals in evals.items():
                if state == 'urtpubsubpublishernrt':
                    outfile = open(os.path.join(outpath, outfiles_bases['pubsubpublishernrt'] + "_" + str(publishers).zfill(4) + outfiles_ext), 'w')
                elif state == 'urtpubsubpublishersrt':
                    outfile = open(os.path.join(outpath, outfiles_bases['pubsubpublishersrt'] + "_" + str(publishers).zfill(4) + outfiles_ext), 'w')
                elif state == 'urtpubsubpublisherfrt':
                    outfile = open(os.path.join(outpath, outfiles_bases['pubsubpublisherfrt'] + "_" + str(publishers).zfill(4) + outfiles_ext), 'w')
                elif state == 'urtpubsubpublisherhrt':
                    outfile = open(os.path.join(outpath, outfiles_bases['pubsubpublisherhrt'] + "_" + str(publishers).zfill(4) + outfiles_ext), 'w')
                outfile.write(header)
                for publisher, values in publisher_evals.items():
                    outfile.write(str(publishers) + "\t" + str(publisher) + "\t" + 
                                  str(values['min']) + "\t" + str(values['max']) + "\t" +
                                  str(values['mean']) + "\t" + str(values['median']) + "\t" +
                                  str(values['jitter']) + "\n")
                outfile.close()
            if state == 'urtpubsubpublishernrt':
                outfile = open(os.path.join(outpath, outfiles_bases['pubsubpublishernrt'] + outfiles_ext), 'w')
            elif state == 'urtpubsubpublishersrt':
                outfile = open(os.path.join(outpath, outfiles_bases['pubsubpublishersrt'] + outfiles_ext), 'w')
            elif state == 'urtpubsubpublisherfrt':
                outfile = open(os.path.join(outpath, outfiles_bases['pubsubpublisherfrt'] + outfiles_ext), 'w')
            elif state == 'urtpubsubpublisherhrt':
                outfile = open(os.path.join(outpath, outfiles_bases['pubsubpublisherhrt'] + outfiles_ext), 'w')
            outfile.write(header)
            for publishers, publisher_evals in evals.items():
                worst = 0
                worstval = 0
                for publisher, values in publisher_evals.items():
                    if values['mean'] > worstval:
                        worst = publisher
                        worstval = values['mean']
                values = publisher_evals[worst]
                outfile.write(str(publishers) + "\t" + str(worst) + "\t" +
                              str(values['min']) + "\t" + str(values['max']) + "\t" +
                              str(values['mean']) + "\t" + str(values['median']) + "\t" +
                              str(values['jitter']) + "\n")
            outfile.close()
            print("done")
            state = 'searchpubsub'
    
    # µRT publish-subscribe subscriber benchmarks
    elif state == 'urtpubsubsubscribernrt' or state == 'urtpubsubsubscribersrt' or state == 'urtpubsubsubscriberfrt' or state == 'urtpubsubsubscriberhrt':
        if re.match('SUBSCRIBERS\tSUBSCRIBER\tITERATION\tLATENCY', line):
            subscribers = 1
            subscriber = 1
            latencies = np.empty(shape=0, dtype=np.int32)
            evals = {subscribers: {subscriber: {}}}
        elif re.match(r'\s*\d+\s+\d+\s+\d+\s+\d+', line):
            values = np.array(re.findall(r'\d+', line), dtype=np.int32)
            if values[0] == subscribers and values[1] == subscriber:
                latencies = np.append(latencies, values[3])
            else:
                evals[subscribers][subscriber] = {'min': np.amin(latencies),
                                                  'max': np.amax(latencies),
                                                  'mean': np.mean(latencies),
                                                  'median': np.median(latencies),
                                                  'jitter': np.amax(latencies) - np.amin(latencies)}
                if values[0] != subscribers:
                    evals[values[0]] = {values[1]: {}}
                subscribers = values[0]
                subscriber = values[1]
                latencies = np.array(values[3], dtype=np.int32)
        elif len(line.strip()) == 0:
            evals[subscribers][subscriber] = {'min': np.amin(latencies),
                                              'max': np.amax(latencies),
                                              'mean': np.mean(latencies),
                                              'median': np.median(latencies),
                                              'jitter': np.amax(latencies) - np.amin(latencies)}
            header = "subscribers\tsubscriber\tmin\tmax\tmean\tmedian\tjitter\n"
            for subscribers, subscriber_evals in evals.items():
                if state == 'urtpubsubsubscribernrt':
                    outfile = open(os.path.join(outpath, outfiles_bases['pubsubsubscribernrt'] + "_" + str(subscribers).zfill(4) + outfiles_ext), 'w')
                elif state == 'urtpubsubsubscribersrt':
                    outfile = open(os.path.join(outpath, outfiles_bases['pubsubsubscribersrt'] + "_" + str(subscribers).zfill(4) + outfiles_ext), 'w')
                elif state == 'urtpubsubsubscriberfrt':
                    outfile = open(os.path.join(outpath, outfiles_bases['pubsubsubscriberfrt'] + "_" + str(subscribers).zfill(4) + outfiles_ext), 'w')
                elif state == 'urtpubsubsubscriberhrt':
                    outfile = open(os.path.join(outpath, outfiles_bases['pubsubsubscriberhrt'] + "_" + str(subscribers).zfill(4) + outfiles_ext), 'w')
                outfile.write(header)
                for subscriber, values in subscriber_evals.items():
                    outfile.write(str(subscribers) + "\t" + str(subscriber) + "\t" + 
                                  str(values['min']) + "\t" + str(values['max']) + "\t" +
                                  str(values['mean']) + "\t" + str(values['median']) + "\t" +
                                  str(values['jitter']) + "\n")
                outfile.close()
            if state == 'urtpubsubsubscribernrt':
                outfile = open(os.path.join(outpath, outfiles_bases['pubsubsubscribernrt'] + outfiles_ext), 'w')
            elif state == 'urtpubsubsubscribersrt':
                outfile = open(os.path.join(outpath, outfiles_bases['pubsubsubscribersrt'] + outfiles_ext), 'w')
            elif state == 'urtpubsubsubscriberfrt':
                outfile = open(os.path.join(outpath, outfiles_bases['pubsubsubscriberfrt'] + outfiles_ext), 'w')
            elif state == 'urtpubsubsubscriberhrt':
                outfile = open(os.path.join(outpath, outfiles_bases['pubsubsubscriberhrt'] + outfiles_ext), 'w')
            outfile.write(header)
            for subscribers, subscriber_evals in evals.items():
                worst = 0
                worstval = 0
                for subscriber, values in subscriber_evals.items():
                    if values['mean'] > worstval:
                        worst = subscriber
                        worstval = values['mean']
                values = subscriber_evals[worst]
                outfile.write(str(subscribers) + "\t" + str(worst) + "\t" +
                              str(values['min']) + "\t" + str(values['max']) + "\t" +
                              str(values['mean']) + "\t" + str(values['median']) + "\t" +
                              str(values['jitter']) + "\n")
            outfile.close()
            print("done")
            state = 'searchpubsub'
    
    # µRT publish-subscribe topic benchmark
    elif state == 'urtpubsubtopicnrt' or state == 'urtpubsubtopicsrt' or state == 'urtpubsubtopicfrt' or state == 'urtpubsubtopichrt':
        if re.match('TOPICS\tTOPIC\tITERATION\tLATENCY', line):
            topics = 1
            topic = 1
            latencies = np.empty(shape=0, dtype=np.int32)
            evals = {topics: {topic: {}}}
        elif re.match(r'\s*\d+\s+\d+\s+\d+\s+\d+', line):
            values = np.array(re.findall(r'\d+', line), dtype=np.int32)
            if values[0] == topics and values[1] == topic:
                latencies = np.append(latencies, values[3])
            else:
                evals[topics][topic] = {'min': np.amin(latencies),
                                        'max': np.amax(latencies),
                                        'mean': np.mean(latencies),
                                        'median': np.median(latencies),
                                        'jitter': np.amax(latencies) - np.amin(latencies)}
                if values[0] != topics:
                    evals[values[0]] = {values[1]: {}}
                topics = values[0]
                topic = values[1]
                latencies = np.array(values[3], dtype=np.int32)
        elif len(line.strip()) == 0:
            evals[topics][topic] = {'min': np.amin(latencies),
                                    'max': np.amax(latencies),
                                    'mean': np.mean(latencies),
                                    'median': np.median(latencies),
                                    'jitter': np.amax(latencies) - np.amin(latencies)}
            header = "topics\ttopic\tmin\tmax\tmean\tmedian\tjitter\n"
            for topics, topic_evals in evals.items():
                if state == 'urtpubsubtopicnrt':
                    outfile = open(os.path.join(outpath, outfiles_bases['pubsubtopicnrt'] + "_" + str(topics).zfill(4) + outfiles_ext), 'w')
                elif state == 'urtpubsubtopicsrt':
                    outfile = open(os.path.join(outpath, outfiles_bases['pubsubtopicsrt'] + "_" + str(topics).zfill(4) + outfiles_ext), 'w')
                elif state == 'urtpubsubtopicfrt':
                    outfile = open(os.path.join(outpath, outfiles_bases['pubsubtopicfrt'] + "_" + str(topics).zfill(4) + outfiles_ext), 'w')
                elif state == 'urtpubsubtopichrt':
                    outfile = open(os.path.join(outpath, outfiles_bases['pubsubtopichrt'] + "_" + str(topics).zfill(4) + outfiles_ext), 'w')
                outfile.write(header)
                for topic, values in topic_evals.items():
                    outfile.write(str(topics) + "\t" + str(topic) + "\t" + 
                                  str(values['min']) + "\t" + str(values['max']) + "\t" +
                                  str(values['mean']) + "\t" + str(values['median']) + "\t" +
                                  str(values['jitter']) + "\n")
                outfile.close()
            if state == 'urtpubsubtopicnrt':
                outfile = open(os.path.join(outpath, outfiles_bases['pubsubtopicnrt'] + outfiles_ext), 'w')
            elif state == 'urtpubsubtopicsrt':
                outfile = open(os.path.join(outpath, outfiles_bases['pubsubtopicsrt'] + outfiles_ext), 'w')
            elif state == 'urtpubsubtopicfrt':
                outfile = open(os.path.join(outpath, outfiles_bases['pubsubtopicfrt'] + outfiles_ext), 'w')
            elif state == 'urtpubsubtopichrt':
                outfile = open(os.path.join(outpath, outfiles_bases['pubsubtopichrt'] + outfiles_ext), 'w')
            outfile.write(header)
            for topics, topic_evals in evals.items():
                worst = 0
                worstval = 0
                for topic, values in topic_evals.items():
                    if values['mean'] > worstval:
                        worst = topic
                        worstval = values['mean']
                values = topic_evals[worst]
                outfile.write(str(topics) + "\t" + str(topic) + "\t" + 
                              str(values['min']) + "\t" + str(values['max']) + "\t" +
                              str(values['mean']) + "\t" + str(values['median']) + "\t" +
                              str(values['jitter']) + "\n")
            outfile.close()
            print("done")
            state = 'searchpubsub'
    
    # µRT publish-subscribe deadline verification
    elif state == 'urtpubsubqosdeadline':
        if re.match(r'critical latency: \d+ us', line):
            crit = int(re.search(r'\d+', line).group())
        elif re.match('STEP\tITERATION\tLATENCY', line):
            step = 1
            latencies = np.empty(shape=0, dtype=np.int32)
            evals = {step: {}}
        elif re.match(r'\s*\d+\s+\d+\s+\d+', line):
            values = np.array(re.findall(r'\d+', line), dtype=np.int32)
            if values[0] == step:
                latencies = np.append(latencies, values[2])
            else:
                evals[step] = {'min': np.amin(latencies),
                               'max': np.amax(latencies),
                               'mean': np.mean(latencies),
                               'median': np.median(latencies),
                               'jitter': np.amax(latencies) - np.amin(latencies)}
                step = values[0]
                latencies = np.array(values[2], dtype=np.int32)
        elif re.match(r'\d+ deadline violations', line) or len(line.strip()) == 0:
            evals[step] = {'min': np.amin(latencies),
                           'max': np.amax(latencies),
                           'mean': np.mean(latencies),
                           'median': np.median(latencies),
                           'jitter': np.amax(latencies) - np.amin(latencies)}
            header = "critical\tstep\tmin\tmax\tmean\tmedian\tjitter\n"
            outfile = open(os.path.join(outpath, outfiles_bases['pubsubqosdeadline'] + outfiles_ext), 'w')
            outfile.write(header)
            for step, values in evals.items():
                outfile.write(str(crit) + "\t" + str(step) + "\t" +
                              str(values['min']) + "\t" + str(values['max']) + "\t" +
                              str(values['mean']) + "\t" + str(values['median']) + "\t" +
                              str(values['jitter']) + "\n")
            outfile.close()
            print("done")
            if len(line.strip()) > 0 and int(re.match(r'\d+', line).group()) == 0:
                print("WARNING: no deadline violations!")
            state = 'searchpubsub'

    # µRT publish-subscribe jitter verification
    elif state == 'urtpubsubqosjitter':
        if re.match(r'critical jitter: \d+ us', line):
            crit = int(re.search(r'\d+', line).group())
        elif re.match('STEP\tITERATION\tLATENCY\tJITTER', line):
            step = 1
            latencies = np.empty(shape=0, dtype=np.int32)
            jitters = np.empty(shape=0, dtype=np.int32)
            evals = {step: {}}
        elif re.match(r'\s*\d+\s+\d+\s+\d+\s+\d+', line):
            values = np.array(re.findall(r'\d+', line), dtype=np.int32)
            if values[0] == step:
                latencies = np.append(latencies, values[2])
                jitters = np.append(jitters, values[3])
            else:
                evals[step] = {'latency': {'min': np.amin(latencies),
                                           'max': np.amax(latencies),
                                           'mean': np.mean(latencies),
                                           'median': np.median(latencies),
                                           'jitter': np.amax(latencies) - np.amin(latencies)},
                               'jitter': {'min': np.amin(jitters),
                                          'max': np.amax(jitters),
                                          'mean': np.mean(jitters),
                                          'median': np.median(jitters),
                                          'jitter': np.amax(jitters) - np.amin(jitters)}}
                step = values[0]
                latencies = np.array(values[2], dtype=np.int32)
                jitters = np.array(values[3], dtype=np.int32)
        elif re.match(r'\d+ jitter violations', line) or len(line.strip()) == 0:
            evals[step] = {'latency': {'min': np.amin(latencies),
                                       'max': np.amax(latencies),
                                       'mean': np.mean(latencies),
                                       'median': np.median(latencies),
                                       'jitter': np.amax(latencies) - np.amin(latencies)},
                           'jitter': {'min': np.amin(jitters),
                                      'max': np.amax(jitters),
                                      'mean': np.mean(jitters),
                                      'median': np.median(jitters),
                                      'jitter': np.amax(jitters) - np.amin(jitters)}}
            header = "critical\tstep\tlatency:min\tlatency:max\tlatency:mean\tlatency:median\tlatency:jitter\tjitter:min\tjitter:max\tjitter:mean\tjitter:median\tjitter:jitter\n"
            outfile = open(os.path.join(outpath, outfiles_bases['pubsubqosjitter'] + outfiles_ext), 'w')
            outfile.write(header)
            for step, values in evals.items():
                outfile.write(str(crit) + "\t" + str(step) + "\t" +
                              str(values['latency']['min']) + "\t" + str(values['latency']['max']) + "\t" +
                              str(values['latency']['mean']) + "\t" + str(values['latency']['median']) + "\t" +
                              str(values['latency']['jitter']) + "\t" +
                              str(values['jitter']['min']) + "\t" + str(values['jitter']['max']) + "\t" +
                              str(values['jitter']['mean']) + "\t" + str(values['jitter']['median']) + "\t" +
                              str(values['jitter']['jitter']) + "\n")
            outfile.close()
            print("done")
            if len(line.strip()) > 0 and int(re.match(r'\d+', line).group()) == 0:
                print("WARNING: no jitter violations!")
            state = 'searchpubsub'
    
    # µRT publish-subscribe rate verification
    elif state == 'urtpubsubqosrate':
        if re.match(r'critical rate: \d+ us', line):
            crit = int(re.search(r'\d+', line).group())
        elif re.match('STEP\tITERATION\tLATENCY', line):
            step = 1
            latencies = np.empty(shape=0, dtype=np.int32)
            evals = {step: {}}
        elif re.match(r'\s*\d+\s+\d+\s+\d+', line):
            values = np.array(re.findall(r'\d+', line), dtype=np.int32)
            if values[0] == step:
                latencies = np.append(latencies, values[2])
            else:
                evals[step] = {'min': np.amin(latencies),
                               'max': np.amax(latencies),
                               'mean': np.mean(latencies),
                               'median': np.median(latencies),
                               'jitter': np.amax(latencies) - np.amin(latencies)}
                step = values[0]
                latencies = np.array(values[2], dtype=np.int32)
        elif re.match(r'\d+ deadline violations', line) or len(line.strip()) == 0:
            evals[step] = {'min': np.amin(latencies),
                           'max': np.amax(latencies),
                           'mean': np.mean(latencies),
                           'median': np.median(latencies),
                           'jitter': np.amax(latencies) - np.amin(latencies)}
            header = "critical\tstep\tmin\tmax\tmean\tmedian\tjitter\n"
            outfile = open(os.path.join(outpath, outfiles_bases['pubsubqosrate'] + outfiles_ext), 'w')
            outfile.write(header)
            for step, values in evals.items():
                outfile.write(str(crit) + "\t" + str(step) + "\t" +
                              str(values['min']) + "\t" + str(values['max']) + "\t" +
                              str(values['mean']) + "\t" + str(values['median']) + "\t" +
                              str(values['jitter']) + "\n")
            outfile.close()
            print("done")
            if len(line.strip()) > 0 and int(re.match(r'\d+', line).group()) == 0:
                print("WARNING: no rate violations!")
            state = 'searchpubsub'
    
    # µRT RPC payload benchmarks
    elif state == 'urtrpcpayloadnrt' or state == 'urtrpcpayloadsrt' or state == 'urtrpcpayloadfrt' or state == 'urtrpcpayloadhrt':
        if re.match('PAYLOAD\tITERATION\tDISPATCH\tRETRIEVE', line):
            payload = 0
            dispatches = np.empty(shape=0, dtype=np.int32)
            responses = np.empty(shape=0, dtype=np.int32)
            retrievals = np.empty(shape=0, dtype=np.int32)
            evals = {payload: {}}
        elif re.match(r'\s*\d+\s+\d+\s+\d+\s+\d+', line):
            values = np.array(re.findall(r'\d+', line), dtype=np.int32)
            if values[0] == payload:
                dispatches = np.append(dispatches, values[2])
                responses = np.append(responses, values[3] - values[2])
                retrievals = np.append(retrievals, values[3])
            else:
                evals[payload] = {'dispatch': {'min': np.amin(dispatches),
                                               'max': np.amax(dispatches),
                                               'mean': np.mean(dispatches),
                                               'median': np.median(dispatches),
                                               'jitter': np.amax(dispatches) - np.amin(dispatches)},
                                  'response': {'min': np.amin(responses),
                                               'max': np.amax(responses),
                                               'mean': np.mean(responses),
                                               'median': np.median(responses),
                                               'jitter': np.amax(responses) - np.amin(responses)},
                                  'retrieve': {'min': np.amin(retrievals),
                                               'max': np.amax(retrievals),
                                               'mean': np.mean(retrievals),
                                               'median': np.median(retrievals),
                                               'jitter': np.amax(retrievals) - np.amin(retrievals)}}
                payload = values[0]
                dispatches = np.array(values[2], dtype=np.int32)
                responses = np.array(values[3] - values[2], dtype=np.int32)
                retrievals = np.array(values[3], dtype=np.int32)
        elif len(line.strip()) == 0:
            evals[payload] = {'dispatch': {'min': np.amin(dispatches),
                                           'max': np.amax(dispatches),
                                           'mean': np.mean(dispatches),
                                           'median': np.median(dispatches),
                                           'jitter': np.amax(dispatches) - np.amin(dispatches)},
                              'response': {'min': np.amin(responses),
                                           'max': np.amax(responses),
                                           'mean': np.mean(responses),
                                           'median': np.median(responses),
                                           'jitter': np.amax(responses) - np.amin(responses)},
                              'retrieve': {'min': np.amin(retrievals),
                                           'max': np.amax(retrievals),
                                           'mean': np.mean(retrievals),
                                           'median': np.median(retrievals),
                                           'jitter': np.amax(retrievals) - np.amin(retrievals)}}
            header = "payload\tdispatch:min\tdispatch:max\tdispatch:mean\tdispatch:median\tdispatch:jitter\tresponse:min\tresponse:max\tresponse:mean\tresponse:median\tresponse:jitter\tretrieve:min\tretrieve:max\tretrieve:mean\tretrieve:median\tretrieve:jitter\n"
            if state == 'urtrpcpayloadnrt':
                outfile = open(os.path.join(outpath, outfiles_bases['rpcpayloadnrt'] + outfiles_ext), 'w')
            elif state == 'urtrpcpayloadsrt':
                outfile = open(os.path.join(outpath, outfiles_bases['rpcpayloadsrt'] + outfiles_ext), 'w')
            elif state == 'urtrpcpayloadfrt':
                outfile = open(os.path.join(outpath, outfiles_bases['rpcpayloadfrt'] + outfiles_ext), 'w')
            elif state == 'urtrpcpayloadhrt':
                outfile = open(os.path.join(outpath, outfiles_bases['rpcpayloadhrt'] + outfiles_ext), 'w')
            outfile.write(header)
            for payload, values in evals.items():
                outfile.write(str(payload) + "\t" +
                              str(values['dispatch']['min']) + "\t" + str(values['dispatch']['max']) + "\t" +
                              str(values['dispatch']['mean']) + "\t" + str(values['dispatch']['median']) + "\t" +
                              str(values['dispatch']['jitter']) + "\t" +
                              str(values['response']['min']) + "\t" + str(values['response']['max']) + "\t" +
                              str(values['response']['mean']) + "\t" + str(values['response']['median']) + "\t" +
                              str(values['response']['jitter']) + "\t" +
                              str(values['retrieve']['min']) + "\t" + str(values['retrieve']['max']) + "\t" +
                              str(values['retrieve']['mean']) + "\t" + str(values['retrieve']['median']) + "\t" +
                              str(values['retrieve']['jitter']) + "\n")
            outfile.close()
            print("done")
            state = 'searchrpc'
    
    # µRT RPC request benchmarks
    elif state == 'urtrpcrequestnrt' or state == 'urtrpcrequestsrt' or state == 'urtrpcrequestfrt' or state == 'urtrpcrequesthrt':
        if re.match('REQUESTS\tREQUEST\tITERATION\tDISPATCH\tRETRIEVE', line):
            requests = 1
            request = 1
            dispatches = np.empty(shape=0, dtype=np.int32)
            responses = np.empty(shape=0, dtype=np.int32)
            retrievals = np.empty(shape=0, dtype=np.int32)
            evals = {requests: {request: {}}}
        elif re.match(r'\s*\d+\s+\d+\s+\d+\s+\d+\s+\d+', line):
            values = np.array(re.findall(r'\d+', line), dtype=np.int32)
            if values[0] == requests and values[1] == request:
                dispatches = np.append(dispatches, values[3])
                responses = np.append(responses, values[4] - values[3])
                retrievals = np.append(retrievals, values[4])
            else:
                evals[requests][request] = {'dispatch': {'min': np.amin(dispatches),
                                                         'max': np.amax(dispatches),
                                                         'mean': np.mean(dispatches),
                                                         'median': np.median(dispatches),
                                                         'jitter': np.amax(dispatches) - np.amin(dispatches)},
                                            'response': {'min': np.amin(responses),
                                                         'max': np.amax(responses),
                                                         'mean': np.mean(responses),
                                                         'median': np.median(responses),
                                                         'jitter': np.amax(responses) - np.amin(responses)},
                                            'retrieve': {'min': np.amin(retrievals),
                                                         'max': np.amax(retrievals),
                                                         'mean': np.mean(retrievals),
                                                         'median': np.median(retrievals),
                                                         'jitter': np.amax(retrievals) - np.amin(retrievals)}}
                if values[0] != requests:
                    evals[values[0]] = {values[1]: {}}
                requests = values[0]
                request = values[1]
                dispatches = np.array(values[3], dtype=np.int32)
                responses = np.array(values[4] - values[3], dtype=np.int32)
                retrievals = np.array(values[4], dtype=np.int32)
        elif len(line.strip()) == 0:
            evals[requests][request] = {'dispatch': {'min': np.amin(dispatches),
                                                     'max': np.amax(dispatches),
                                                     'mean': np.mean(dispatches),
                                                     'median': np.median(dispatches),
                                                     'jitter': np.amax(dispatches) - np.amin(dispatches)},
                                        'response': {'min': np.amin(responses),
                                                     'max': np.amax(responses),
                                                     'mean': np.mean(responses),
                                                     'median': np.median(responses),
                                                     'jitter': np.amax(responses) - np.amin(responses)},
                                        'retrieve': {'min': np.amin(retrievals),
                                                     'max': np.amax(retrievals),
                                                     'mean': np.mean(retrievals),
                                                     'median': np.median(retrievals),
                                                     'jitter': np.amax(retrievals) - np.amin(retrievals)}}
            header = "requests\trequest\tdispatch:min\tdispatch:max\tdispatch:mean\tdispatch:median\tdispatch:jitter\tresponse:min\tresponse:max\tresponse:mean\tresponse:median\tresponse:jitter\tretrieve:min\tretrieve:max\tretrieve:mean\tretrieve:median\tretrieve:jitter\n"
            for requests, request_evals in evals.items():
                if state == 'urtrpcrequestnrt':
                    outfile = open(os.path.join(outpath, outfiles_bases['rpcrequestnrt'] + "_" + str(requests).zfill(4) + outfiles_ext), 'w')
                elif state == 'urtrpcrequestsrt':
                    outfile = open(os.path.join(outpath, outfiles_bases['rpcrequestsrt'] + "_" + str(requests).zfill(4) + outfiles_ext), 'w')
                elif state == 'urtrpcrequestfrt':
                    outfile = open(os.path.join(outpath, outfiles_bases['rpcrequestfrt'] + "_" + str(requests).zfill(4) + outfiles_ext), 'w')
                elif state == 'urtrpcrequesthrt':
                    outfile = open(os.path.join(outpath, outfiles_bases['rpcrequesthrt'] + "_" + str(requests).zfill(4) + outfiles_ext), 'w')
                outfile.write(header)
                for request, values in request_evals.items():
                    outfile.write(str(requests) + "\t" + str(request) + "\t"+
                                  str(values['dispatch']['min']) + "\t" + str(values['dispatch']['max']) + "\t" +
                                  str(values['dispatch']['mean']) + "\t" + str(values['dispatch']['median']) + "\t" +
                                  str(values['dispatch']['jitter']) + "\t" +
                                  str(values['response']['min']) + "\t" + str(values['response']['max']) + "\t" +
                                  str(values['response']['mean']) + "\t" + str(values['response']['median']) + "\t" +
                                  str(values['response']['jitter']) + "\t" +
                                  str(values['retrieve']['min']) + "\t" + str(values['retrieve']['max']) + "\t" +
                                  str(values['retrieve']['mean']) + "\t" + str(values['retrieve']['median']) + "\t" +
                                  str(values['retrieve']['jitter']) + "\n")
                outfile.close()
            for latencytype in ['dispatch', 'response', 'retrieve']:
                if state == 'urtrpcrequestnrt':
                    outfile = open(os.path.join(outpath, outfiles_bases['rpcrequestnrt'] + "_" + latencytype + outfiles_ext), 'w')
                elif state == 'urtrpcrequestsrt':
                    outfile = open(os.path.join(outpath, outfiles_bases['rpcrequestsrt'] + "_" + latencytype + outfiles_ext), 'w')
                elif state == 'urtrpcrequestfrt':
                    outfile = open(os.path.join(outpath, outfiles_bases['rpcrequestfrt'] + "_" + latencytype + outfiles_ext), 'w')
                elif state == 'urtrpcrequesthrt':
                    outfile = open(os.path.join(outpath, outfiles_bases['rpcrequesthrt'] + "_" + latencytype + outfiles_ext), 'w')
                outfile.write(header)
                for requests, request_evals in evals.items():
                    worst = 0
                    worstval = 0
                    for request, values in request_evals.items():
                        if values[latencytype]['mean'] > worstval:
                            worst = request
                            values[latencytype]['mean']
                    values = request_evals[worst]
                    outfile.write(str(requests) + "\t" + str(request) + "\t"+
                                  str(values['dispatch']['min']) + "\t" + str(values['dispatch']['max']) + "\t" +
                                  str(values['dispatch']['mean']) + "\t" + str(values['dispatch']['median']) + "\t" +
                                  str(values['dispatch']['jitter']) + "\t" +
                                  str(values['response']['min']) + "\t" + str(values['response']['max']) + "\t" +
                                  str(values['response']['mean']) + "\t" + str(values['response']['median']) + "\t" +
                                  str(values['response']['jitter']) + "\t" +
                                  str(values['retrieve']['min']) + "\t" + str(values['retrieve']['max']) + "\t" +
                                  str(values['retrieve']['mean']) + "\t" + str(values['retrieve']['median']) + "\t" +
                                  str(values['retrieve']['jitter']) + "\n")
                outfile.close()
            print("done")
            state = 'searchrpc'

    # µRT RPC service benchmarks
    elif state == 'urtrpcservicenrt' or state == 'urtrpcservicesrt' or state == 'urtrpcservicefrt' or state == 'urtrpcservicehrt':
        if re.match('SERVICES\tSERVICE\tITERATION\tDISPATCH\tRETRIEVE', line):
            services = 1
            service = 1
            dispatches = np.empty(shape=0, dtype=np.int32)
            responses = np.empty(shape=0, dtype=np.int32)
            retrievals = np.empty(shape=0, dtype=np.int32)
            evals = {services: {service: {}}}
        elif re.match(r'\s*\d+\s+\d+\s+\d+\s+\d+\s+\d+', line):
            values = np.array(re.findall(r'\d+', line), dtype=np.int32)
            if values[0] == services and values[1] == service:
                dispatches = np.append(dispatches, values[3])
                responses = np.append(responses, values[4] - values[3])
                retrievals = np.append(retrievals, values[4])
            else:
                evals[services][service] = {'dispatch': {'min': np.amin(dispatches),
                                                         'max': np.amax(dispatches),
                                                         'mean': np.mean(dispatches),
                                                         'median': np.median(dispatches),
                                                         'jitter': np.amax(dispatches) - np.amin(dispatches)},
                                            'response': {'min': np.amin(responses),
                                                         'max': np.amax(responses),
                                                         'mean': np.mean(responses),
                                                         'median': np.median(responses),
                                                         'jitter': np.amax(responses) - np.amin(responses)},
                                            'retrieve': {'min': np.amin(retrievals),
                                                         'max': np.amax(retrievals),
                                                         'mean': np.mean(retrievals),
                                                         'median': np.median(retrievals),
                                                         'jitter': np.amax(retrievals) - np.amin(retrievals)}}
                if values[0] != services:
                    evals[values[0]] = {values[1]: {}}
                services = values[0]
                service = values[1]
                dispatches = np.array(values[3], dtype=np.int32)
                responses = np.array(values[4] - values[3], dtype=np.int32)
                retrievals = np.array(values[4], dtype=np.int32)
        elif len(line.strip()) == 0:
            evals[services][service] = {'dispatch': {'min': np.amin(dispatches),
                                                     'max': np.amax(dispatches),
                                                     'mean': np.mean(dispatches),
                                                     'median': np.median(dispatches),
                                                     'jitter': np.amax(dispatches) - np.amin(dispatches)},
                                        'response': {'min': np.amin(responses),
                                                     'max': np.amax(responses),
                                                     'mean': np.mean(responses),
                                                     'median': np.median(responses),
                                                     'jitter': np.amax(responses) - np.amin(responses)},
                                        'retrieve': {'min': np.amin(retrievals),
                                                     'max': np.amax(retrievals),
                                                     'mean': np.mean(retrievals),
                                                     'median': np.median(retrievals),
                                                     'jitter': np.amax(retrievals) - np.amin(retrievals)}}
            header = "services\tservice\tdispatch:min\tdispatch:max\tdispatch:mean\tdispatch:median\tdispatch:jitter\tresponse:min\tresponse:max\tresponse:mean\tresponse:median\tresponse:jitter\tretrieve:min\tretrieve:max\tretrieve:mean\tretrieve:median\tretrieve:jitter\n"
            for services, service_evals in evals.items():
                if state == 'urtrpcservicenrt':
                    outfile = open(os.path.join(outpath, outfiles_bases['rpcservicenrt'] + "_" + str(services).zfill(4) + outfiles_ext), 'w')
                elif state == 'urtrpcservicesrt':
                    outfile = open(os.path.join(outpath, outfiles_bases['rpcservicesrt'] + "_" + str(services).zfill(4) + outfiles_ext), 'w')
                elif state == 'urtrpcservicefrt':
                    outfile = open(os.path.join(outpath, outfiles_bases['rpcservicefrt'] + "_" + str(services).zfill(4) + outfiles_ext), 'w')
                elif state == 'urtrpcservicehrt':
                    outfile = open(os.path.join(outpath, outfiles_bases['rpcservicehrt'] + "_" + str(services).zfill(4) + outfiles_ext), 'w')
                outfile.write(header)
                for service, values in service_evals.items():
                    outfile.write(str(services) + "\t" + str(service) + "\t"+
                                  str(values['dispatch']['min']) + "\t" + str(values['dispatch']['max']) + "\t" +
                                  str(values['dispatch']['mean']) + "\t" + str(values['dispatch']['median']) + "\t" +
                                  str(values['dispatch']['jitter']) + "\t" +
                                  str(values['response']['min']) + "\t" + str(values['response']['max']) + "\t" +
                                  str(values['response']['mean']) + "\t" + str(values['response']['median']) + "\t" +
                                  str(values['response']['jitter']) + "\t" +
                                  str(values['retrieve']['min']) + "\t" + str(values['retrieve']['max']) + "\t" +
                                  str(values['retrieve']['mean']) + "\t" + str(values['retrieve']['median']) + "\t" +
                                  str(values['retrieve']['jitter']) + "\n")
                outfile.close()
            for latencytype in ['dispatch', 'response', 'retrieve']:
                if state == 'urtrpcservicenrt':
                    outfile = open(os.path.join(outpath, outfiles_bases['rpcservicenrt'] + "_" + latencytype + outfiles_ext), 'w')
                elif state == 'urtrpcservicesrt':
                    outfile = open(os.path.join(outpath, outfiles_bases['rpcservicesrt'] + "_" + latencytype + outfiles_ext), 'w')
                elif state == 'urtrpcservicefrt':
                    outfile = open(os.path.join(outpath, outfiles_bases['rpcservicefrt'] + "_" + latencytype + outfiles_ext), 'w')
                elif state == 'urtrpcservicehrt':
                    outfile = open(os.path.join(outpath, outfiles_bases['rpcservicehrt'] + "_" + latencytype + outfiles_ext), 'w')
                outfile.write(header)
                for services, service_evals in evals.items():
                    worst = 0
                    worstval = 0
                    for service, values in service_evals.items():
                        if values[latencytype]['mean'] > worstval:
                            worst = service
                            worstval = values[latencytype]['mean']
                    values = service_evals[worst]
                    outfile.write(str(services) + "\t" + str(service) + "\t"+
                                  str(values['dispatch']['min']) + "\t" + str(values['dispatch']['max']) + "\t" +
                                  str(values['dispatch']['mean']) + "\t" + str(values['dispatch']['median']) + "\t" +
                                  str(values['dispatch']['jitter']) + "\t" +
                                  str(values['response']['min']) + "\t" + str(values['response']['max']) + "\t" +
                                  str(values['response']['mean']) + "\t" + str(values['response']['median']) + "\t" +
                                  str(values['response']['jitter']) + "\t" +
                                  str(values['retrieve']['min']) + "\t" + str(values['retrieve']['max']) + "\t" +
                                  str(values['retrieve']['mean']) + "\t" + str(values['retrieve']['median']) + "\t" +
                                  str(values['retrieve']['jitter']) + "\n")
                outfile.close()
            print("done")
            state = 'searchrpc'
    
    # µRT RPC deadline verification
    elif state == 'urtrpcqosdeadline':
        if re.match(r'critical latency: \d+ us', line):
            crit = int(re.search(r'\d+', line).group())
        elif re.match('STEP\tITERATION\tDISPATCH\tRETRIEVE', line):
            step = 1
            dispatches = np.empty(shape=0, dtype=np.int32)
            responses = np.empty(shape=0, dtype=np.int32)
            retrievals = np.empty(shape=0, dtype=np.int32)
            evals = {step: {}}
        elif re.match(r'\s*\d+\s+\d+\s+\d+\s+\d+', line):
            values = np.array(re.findall(r'\d+', line), dtype=np.int32)
            if values[0] == step:
                dispatches = np.append(dispatches, values[2])
                responses = np.append(responses, values[3] - values[2])
                retrievals = np.append(retrievals, values[3])
            else:
                evals[step] = {'dispatch': {'min': np.amin(dispatches),
                                            'max': np.amax(dispatches),
                                            'mean': np.mean(dispatches),
                                            'median': np.median(dispatches),
                                            'jitter': np.amax(dispatches) - np.amin(dispatches)},
                               'response': {'min': np.amin(responses),
                                            'max': np.amax(responses),
                                            'mean': np.mean(responses),
                                            'median': np.median(responses),
                                            'jitter': np.amax(responses) - np.amin(responses)},
                               'retrieve': {'min': np.amin(retrievals),
                                            'max': np.amax(retrievals),
                                            'mean': np.mean(retrievals),
                                            'median': np.median(retrievals),
                                            'jitter': np.amax(retrievals) - np.amin(retrievals)}}
                step = values[0]
                dispatches = np.array(values[2], dtype=np.int32)
                responses = np.array(values[3] - values[2], dtype=np.int32)
                retrievals = np.array(values[3], dtype=np.int32)
        elif re.match(r'\d+ deadline violations', line) or len(line.strip()) == 0:
            evals[step] = {'dispatch': {'min': np.amin(dispatches),
                                        'max': np.amax(dispatches),
                                        'mean': np.mean(dispatches),
                                        'median': np.median(dispatches),
                                        'jitter': np.amax(dispatches) - np.amin(dispatches)},
                           'response': {'min': np.amin(responses),
                                        'max': np.amax(responses),
                                        'mean': np.mean(responses),
                                        'median': np.median(responses),
                                        'jitter': np.amax(responses) - np.amin(responses)},
                           'retrieve': {'min': np.amin(retrievals),
                                        'max': np.amax(retrievals),
                                        'mean': np.mean(retrievals),
                                        'median': np.median(retrievals),
                                        'jitter': np.amax(retrievals) - np.amin(retrievals)}}
            header = "critical\tstep\tdispatch:min\tdispatch:max\tdispatch:mean\tdispatch:median\tdispatch:jitter\tresponse:min\tresponse:max\tresponse:mean\tresponse:median\tresponse:jitter\tretrieve:min\tretrieve:max\tretrieve:mean\tretrieve:median\tretrieve:jitter\n"
            outfile = open(os.path.join(outpath, outfiles_bases['rpcqosdeadline'] + outfiles_ext), 'w')
            outfile.write(header)
            for step, values in evals.items():
                outfile.write(str(crit) + "\t" + str(step) + "\t" +
                              str(values['dispatch']['min']) + "\t" + str(values['dispatch']['max']) + "\t" +
                              str(values['dispatch']['mean']) + "\t" + str(values['dispatch']['median']) + "\t" +
                              str(values['dispatch']['jitter']) + "\t" +
                              str(values['response']['min']) + "\t" + str(values['response']['max']) + "\t" +
                              str(values['response']['mean']) + "\t" + str(values['response']['median']) + "\t" +
                              str(values['response']['jitter']) + "\t" +
                              str(values['retrieve']['min']) + "\t" + str(values['retrieve']['max']) + "\t" +
                              str(values['retrieve']['mean']) + "\t" + str(values['retrieve']['median']) + "\t" +
                              str(values['retrieve']['jitter']) + "\n")
            outfile.close()
            print("done")
            if len(line.strip()) > 0 and int(re.match(r'\d+', line).group()) == 0:
                print("WARNING: no deadline violations!")
            state = 'searchrpc'
    
    # µRT RPC jitter verification
    elif state == 'urtrpcqosjitter':
        if re.match(r'critical jitter: \d+ us', line):
            crit = int(re.search(r'\d+', line).group())
        elif re.match('STEP\tITERATION\tDISPATCH\tRETRIEVE\tJITTER', line):
            step = 1
            dispatches = np.empty(shape=0, dtype=np.int32)
            responses = np.empty(shape=0, dtype=np.int32)
            retrievals = np.empty(shape=0, dtype=np.int32)
            jitters = np.empty(shape=0, dtype=np.int32)
            evals = {step: {}}
        elif re.match(r'\s*\d+\s+\d+\s+\d+\s+\d+\s+\d+', line):
            values = np.array(re.findall(r'\d+', line), dtype=np.int32)
            if values[0] == step:
                dispatches = np.append(dispatches, values[2])
                responses = np.append(responses, values[3] - values[2])
                retrievals = np.append(retrievals, values[3])
                jitters = np.append(jitters, values[4])
            else:
                evals[step] = {'dispatch': {'min': np.amin(dispatches),
                                            'max': np.amax(dispatches),
                                            'mean': np.mean(dispatches),
                                            'median': np.median(dispatches),
                                            'jitter': np.amax(dispatches) - np.amin(dispatches)},
                               'response': {'min': np.amin(responses),
                                            'max': np.amax(responses),
                                            'mean': np.mean(responses),
                                            'median': np.median(responses),
                                            'jitter': np.amax(responses) - np.amin(responses)},
                               'retrieve': {'min': np.amin(retrievals),
                                            'max': np.amax(retrievals),
                                            'mean': np.mean(retrievals),
                                            'median': np.median(retrievals),
                                            'jitter': np.amax(retrievals) - np.amin(retrievals)},
                               'jitter': {'min': np.amin(jitters),
                                          'max': np.amax(jitters),
                                          'mean': np.mean(jitters),
                                          'median': np.median(jitters),
                                          'jitter': np.amax(jitters) - np.amin(jitters)}}
                step = values[0]
                dispatches = np.array(values[2], dtype=np.int32)
                responses = np.array(values[3] - values[2], dtype=np.int32)
                retrievals = np.array(values[3], dtype=np.int32)
                jitters = np.array(values[4], dtype=np.int32)
        elif re.match(r'\d+ jitter violations', line) or len(line.strip()) == 0:
            evals[step] = {'dispatch': {'min': np.amin(dispatches),
                                        'max': np.amax(dispatches),
                                        'mean': np.mean(dispatches),
                                        'median': np.median(dispatches),
                                        'jitter': np.amax(dispatches) - np.amin(dispatches)},
                           'response': {'min': np.amin(responses),
                                        'max': np.amax(responses),
                                        'mean': np.mean(responses),
                                        'median': np.median(responses),
                                        'jitter': np.amax(responses) - np.amin(responses)},
                           'retrieve': {'min': np.amin(retrievals),
                                        'max': np.amax(retrievals),
                                        'mean': np.mean(retrievals),
                                        'median': np.median(retrievals),
                                        'jitter': np.amax(retrievals) - np.amin(retrievals)},
                           'jitter': {'min': np.amin(jitters),
                                      'max': np.amax(jitters),
                                      'mean': np.mean(jitters),
                                      'median': np.median(jitters),
                                      'jitter': np.amax(jitters) - np.amin(jitters)}}
            header = "critical\tstep\tdispatch:min\tdispatch:max\tdispatch:mean\tdispatch:median\tdispatch:jitter\tresponse:min\tresponse:max\tresponse:mean\tresponse:median\tresponse:jitter\tretrieve:min\tretrieve:max\tretrieve:mean\tretrieve:median\tretrieve:jitter\n"
            outfile = open(os.path.join(outpath, outfiles_bases['rpcqosjitter'] + outfiles_ext), 'w')
            outfile.write(header)
            for step, values in evals.items():
                outfile.write(str(crit) + "\t" + str(step) + "\t" +
                              str(values['dispatch']['min']) + "\t" + str(values['dispatch']['max']) + "\t" +
                              str(values['dispatch']['mean']) + "\t" + str(values['dispatch']['median']) + "\t" +
                              str(values['dispatch']['jitter']) + "\t" +
                              str(values['response']['min']) + "\t" + str(values['response']['max']) + "\t" +
                              str(values['response']['mean']) + "\t" + str(values['response']['median']) + "\t" +
                              str(values['response']['jitter']) + "\t" +
                              str(values['retrieve']['min']) + "\t" + str(values['retrieve']['max']) + "\t" +
                              str(values['retrieve']['mean']) + "\t" + str(values['retrieve']['median']) + "\t" +
                              str(values['retrieve']['jitter']) + "\t" +
                              str(values['jitter']['min']) + "\t" + str(values['jitter']['max']) + "\t" +
                              str(values['jitter']['mean']) + "\t" + str(values['jitter']['median']) + "\t" +
                              str(values['jitter']['jitter']) + "\n")
            outfile.close()
            print("done")
            if len(line.strip()) > 0 and int(re.match(r'\d+', line).group()) == 0:
                print("WARNING: no jitter violations!")
            state = 'searchrpc'

"""
Close input file and quit script.
"""

infile.close()
quit()
