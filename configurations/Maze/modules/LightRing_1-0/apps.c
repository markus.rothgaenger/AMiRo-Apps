/*
AMiRo-Apps is a collection of applications for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2021  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <apps.h>
#include <canbridge.h>
#include <light.h>
#include <stdlib.h>
#include <string.h>

/******************************************************************************/
/* LOCAL DEFINITIONS                                                          */
/******************************************************************************/

/******************************************************************************/
/* EXPORTED VARIABLES                                                         */
/******************************************************************************/

/******************************************************************************/
/* LOCAL TYPES                                                                */
/******************************************************************************/

/******************************************************************************/
/* LOCAL VARIABLES                                                            */
/******************************************************************************/

/*
 * forward declarations
 */
static int _light_shellcb(BaseSequentialStream* stream, int argc, char* argv[]);

/**
 * @brief   Shell command for proximity testing of CAN and ring.
 */
static AOS_SHELL_COMMAND(_light_shellcmd, "CAN:Test", _light_shellcb);

/**
 * @brief Topic for the ring data
 */
urt_topic_t _proximity_ring_topic;
urt_topic_t _ambient_ring_topic;
ring_sensors_t prox_ring_payload;
ring_sensors_t amb_ring_payload;

urt_topic_t _proximity_floor_topic;
urt_topic_t _ambient_floor_topic;
floor_sensors_t prox_floor_payload;
floor_sensors_t amb_floor_payload;

/**
 * @brief CAN node object of the LightRing.
 */
static can_node_t _can;

/**
 * @brief Light data object
 */
static light_data_t _light;

/******************************************************************************/
/* LOCAL FUNCTIONS                                                            */
/******************************************************************************/

static int _light_shellcb(BaseSequentialStream* stream, int argc, char* argv[])
{
  (void)argc;
  (void)argv;

  chprintf(stream, "Light Callback: %u \n", _light.callback);
  chprintf(stream, "CAN Callback: %u \n", _can.callback_test);

  for (int sensor = 0; sensor < 8; sensor++) {
      chprintf(stream, "Ring %i: %u \t amb: %u \n",
               sensor,
               _can.pub_ring_data[0].ring_data[0].values.data[sensor],
               _can.pub_ring_data[1].ring_data[0].values.data[sensor]);
  }

  for (int sensor = 0; sensor < 4; sensor++) {
      chprintf(stream, "Floor %i: %u \t amb: %u \n",
               sensor,
               _can.pub_floor_data[0].floor_data[0].values.data[sensor],
               _can.pub_floor_data[1].floor_data[0].values.data[sensor]);
  }

  return AOS_OK;
}

/******************************************************************************/
/* EXPORTED FUNCTIONS                                                         */
/******************************************************************************/

/**
 * @brief   Initializes all Apps from the LightRing.
 */
void appsInit(void)
{
  urtTopicInit(&_proximity_ring_topic, PROXIMITY_RING_TOPICID, &prox_ring_payload);
  urtTopicInit(&_ambient_ring_topic, AMBIENT_RING_TOPICID, &amb_ring_payload);
  urtTopicInit(&_proximity_floor_topic, PROXIMITY_FLOOR_TOPICID, &prox_floor_payload);
  urtTopicInit(&_ambient_floor_topic, AMBIENT_FLOOR_TOPICID, &amb_floor_payload);

  lightInit(&_light,
            URT_THREAD_PRIO_NORMAL_MIN,
            LIGHT_SERVICE_ID);

  //Publisher gets data from the ring and publishs them to the ring Topic
  _can.pub_ring_data[0].topicid = PROXIMITY_RING_TOPICID;
  _can.pub_ring_data[0].size = sizeof(ring_sensors_t);

  _can.pub_ring_data[1].topicid = AMBIENT_RING_TOPICID;
  _can.pub_ring_data[1].size = sizeof(ring_sensors_t);

  //Publisher gets data from the floor and publishs them to the floor Topic
  _can.pub_floor_data[0].topicid = PROXIMITY_FLOOR_TOPICID;
  _can.pub_floor_data[0].size = sizeof(floor_sensors_t);

  _can.pub_floor_data[1].topicid = AMBIENT_FLOOR_TOPICID;
  _can.pub_floor_data[1].size = sizeof(floor_sensors_t);

  //Request signals the light service
  _can.request_data.serviceToSignal = &_light.light_service;

  id_map_t ids;
  ids.cnt = 0;
  ids.id =~(_can.pub_ring_data[0].topicid ^ _can.pub_ring_data[1].topicid
           ^ _can.pub_floor_data[0].topicid ^ _can.pub_floor_data[1].topicid ^
           LIGHT_SERVICE_ID);
  ids.type = ~(CAN_TOPIC_DATA ^ CAN_REQUEST);

  id_map_t filter_id;
  filter_id.cnt = 0;
  filter_id.id = _can.pub_ring_data[0].topicid | _can.pub_ring_data[1].topicid
                | _can.pub_floor_data[0].topicid | _can.pub_floor_data[1].topicid
                | LIGHT_SERVICE_ID;
  filter_id.type = CAN_TOPIC_DATA | CAN_REQUEST;

  // specify CAN Bus filter to get only for this app relevant messages
  _can.can_filter.mask.ext.id = ids.raw; //Equal bit of these two masks must match other not
  _can.can_filter.mask.ext.ide = ~0;
  _can.can_filter.mask.ext.rtr = ~0;
  _can.can_filter.filter.ext.id = filter_id.raw; // should only receive messages from this ID
  _can.can_filter.filter.ext.ide = CAN_IDE_EXT;
  _can.can_filter.filter.ext.rtr = CAN_RTR_DATA;
  _can.can_filter.cbparams = &_can;

  canLogicInit(&_can,
               URT_THREAD_PRIO_NORMAL_MIN);

  // add shell commands
  aosShellAddCommand(&_light_shellcmd);

  return;
}
