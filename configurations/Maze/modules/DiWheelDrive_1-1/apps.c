/*
AMiRo-Apps is a collection of applications for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2021  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "apps.h"
#include <math.h>
#include <amiroos.h>
#include <differentialmotionestimator.h>
#include <differentialmotorcontrol.h>
#include <floor.h>
#include <canbridge.h>
#include <stdlib.h>
#include <string.h>

/******************************************************************************/
/* LOCAL DEFINITIONS                                                          */
/******************************************************************************/

#if !defined(M_PI)
#define M_PI                                      3.14159265358979323846
#endif

/**
 * @brief   Wheel diameter.
 * @todo    Should be two values (left & right) stored at EEPROM, so that individual robots can have individual values.
 */
#define WHEEL_DIAMETER                            55710

/**
 * @brief   Wheel diameter.
 * @todo    Should be stored at EEPROM, so that individual robots can have individual values.
 */
#define WHEEL_OFFSET                              (34000 + (9000/2))

/**
 * @brief   Frequency of the DME.
 */
#define DME_FREQUENCY                             100

/**
 * @brief   Topic ID for DME motion information.
 */
#define DME_TOPIC_ID                              5

/**
 * @brief   Service ID for setting DMC target information.
 */
#define DMC_TARGETSERVICE_ID                      7

/**
 * @brief   Service ID for executing DMC auto calibration.
 */
#define DMC_CALIBSERVICE_ID                       8

/******************************************************************************/
/* EXPORTED VARIABLES                                                         */
/******************************************************************************/

/******************************************************************************/
/* LOCAL TYPES                                                                */
/******************************************************************************/

/******************************************************************************/
/* LOCAL VARIABLES                                                            */
/******************************************************************************/

/*
 * forward declarations
 */
static int _can_shellcb(BaseSequentialStream* stream, int argc, char* argv[]);
static int _appsDmcShellCmdCb(BaseSequentialStream* stream, int argc, char* argv[]);

/**
 * @brief   shell command to test the CAN Bridge
 */
static AOS_SHELL_COMMAND(_can_shellcmd, "CAN:test", _can_shellcb);
/**
 * @brief   DMC shell command.
 */
static AOS_SHELL_COMMAND(_appsDmcSehhCmd, "DMC:setVelocity", _appsDmcShellCmdCb);


/**
 * @brief   Topics for the different data.
 */
urt_topic_t _proximity_floor_topic;
urt_topic_t _ambient_floor_topic;
floor_sensors_t _proximity_floor_payload;
floor_sensors_t _ambient_floor_payload;

urt_topic_t _proximity_ring_topic;
urt_topic_t _ambient_ring_topic;
ring_sensors_t _proximity_ring_payload;
ring_sensors_t _ambient_ring_payload;

static urt_topic_t _dme_motion_topic;
static dme_motionpayload_t _dme_motion_topic_payload;

/**
 * @brief   Floor node object.
 */
floor_node_t _floor;

/**
 * @brief proximity driver frequency (in Hz).
 */
const float frequency = 50;

/**
 * @brief CAN node object of the DiWheelDrive.
 */
static can_node_t _can;

/**
 * @brief   DME instance.
 */
static dme_t _dme;

/**
 * @brief   DME configuration.
 */
static const dme_config_t _dme_config = {
  .left = {
    .circumference = (M_PI * WHEEL_DIAMETER) + 0.5f,
    .offset = WHEEL_OFFSET,
    .qei = {
      .driver = &MODULE_HAL_QEI_LEFT_WHEEL,
      .increments_per_revolution = MODULE_HAL_QEI_INCREMENTS_PER_REVOLUTION,
    },
  },
  .right = {
    .circumference = (M_PI * WHEEL_DIAMETER) + 0.5f,
    .offset = WHEEL_OFFSET,
    .qei = {
      .driver = &MODULE_HAL_QEI_RIGHT_WHEEL,
      .increments_per_revolution = MODULE_HAL_QEI_INCREMENTS_PER_REVOLUTION,
    },
  },
  .interval = MICROSECONDS_PER_SECOND / DME_FREQUENCY,
};

/**
 * @brief   DMC instance.
 */
static dmc_t _dmc;

/**
 * @brief   DMC configuration.
 */
static const dmc_config_t _dmc_config = {
  .motors = {
    .left = {
      .forward = {
        .driver = &MODULE_HAL_PWM_DRIVE,
        .channel = MODULE_HAL_PWM_DRIVE_CHANNEL_LEFT_FORWARD,
      },
      .reverse= {
        .driver = &MODULE_HAL_PWM_DRIVE,
        .channel = MODULE_HAL_PWM_DRIVE_CHANNEL_LEFT_BACKWARD,
      },
    },
    .right = {
      .forward = {
        .driver = &MODULE_HAL_PWM_DRIVE,
        .channel = MODULE_HAL_PWM_DRIVE_CHANNEL_RIGHT_FORWARD,
      },
      .reverse= {
        .driver = &MODULE_HAL_PWM_DRIVE,
        .channel = MODULE_HAL_PWM_DRIVE_CHANNEL_RIGHT_BACKWARD,
      },
    },
  },
  .lpf = {
    .factor = 10.0f,
    .max = {
      .steering = 0.0f,
      .left = 0.0f,
      .right = 0.0f,
    },
  },
};


/******************************************************************************/
/* LOCAL FUNCTIONS                                                            */
/******************************************************************************/


#if (AMIROOS_CFG_SHELL_ENABLE == true) || defined(__DOXYGEN__)

/**
 * @brief   DMC shell command callback.
 *
 * @param[in] stream  Shell I/O stream.
 * @param[in] argc    Number of command arguments.
 * @param[in] argv    Argument list.
 *
 * @return  The operation result.
 */
static int _appsDmcShellCmdCb(BaseSequentialStream* stream, int argc, char* argv[])
{
  return dmcShellCallback_setVelocity(stream, argc, argv, urtCoreGetService(DMC_TARGETSERVICE_ID));
}

/**
 * @brief   DMC get gains shell command callback.
 *
 * @param[in] stream  Shell I/O stream.
 * @param[in] argc    Number of arguments.
 * @param[in] argv    Argument list.
 *
 * @return  The operation result.
 */
static int _appsDmcShellCmdCb_getGains(BaseSequentialStream* stream, int argc, char* argv[])
{
  return dmcShellCallback_getGains(stream, argc, argv, &_dmc);
}

/**
 * @brief   DMC set gains shell command callback.
 *
 * @param[in] stream  Shell I/O stream.
 * @param[in] argc    Number of arguments.
 * @param[in] argv    Argument list.
 *
 * @return  The operation result.
 */
static int _appsDmcShellCmdCb_setGains(BaseSequentialStream* stream, int argc, char* argv[])
{
  return dmcShellCallback_setGains(stream, argc, argv, &_dmc);
}

/**
 * @brief   DMC set gains shell command.
 */
static AOS_SHELL_COMMAND(_appsDmcShellCmd_setGains, "DMC:setGains", _appsDmcShellCmdCb_setGains);

/**
 * @brief   DMC get gains shell command.
 */
static AOS_SHELL_COMMAND(_appsDmcShellCmd_getGains, "DMC:getGains", _appsDmcShellCmdCb_getGains);

static int _can_shellcb(BaseSequentialStream* stream, int argc, char* argv[])
{
  (void)stream;
  (void)argc;
  (void)argv;

  for (int sensor = 0; sensor < 8; sensor++) {
    urtPrintf("RING data %i: proximity %u %t ambient %u \n",
              sensor,
              _can.pub_ring_data[0].ring_data[0].values.data[sensor],
              _can.pub_ring_data[1].ring_data[0].values.data[sensor]);
  }
  urtPrintf("Timestamp prox: %u \n", _can.pub_ring_data[0].timestamp[0]);

  if (&_dmc.service_targetspeed == NULL) {
    urtPrintf("NULL \n");
  } else {
    urtPrintf("Not NULL \n");
  }

  urtPrintf("CAN Callback: %f \n", _can.callback_test);
  return AOS_OK;
}
#endif /* (AMIROOS_CFG_SHELL_ENABLE == true) || defined(__DOXYGEN__) */

/******************************************************************************/
/* EXPORTED FUNCTIONS                                                         */
/******************************************************************************/

/**
 * @brief   Initializes all Apps from the PowerManagement.
 */
void appsInit(void)
{
  //initialize event sources and timers
  urtTopicInit(&_proximity_floor_topic, PROXIMITY_FLOOR_TOPICID, &_proximity_floor_payload);
  urtTopicInit(&_ambient_floor_topic, AMBIENT_FLOOR_TOPICID, &_ambient_floor_payload);
  urtTopicInit(&_proximity_ring_topic, PROXIMITY_RING_TOPICID, &_proximity_ring_payload);
  urtTopicInit(&_ambient_ring_topic, AMBIENT_RING_TOPICID, &_ambient_ring_payload);
  urtTopicInit(&_dme_motion_topic, DME_TOPIC_ID, &_dme_motion_topic_payload);

  // initialize DME
  dmeInit(&_dme, &_dme_config, DME_TOPIC_ID, URT_THREAD_PRIO_RT_MAX);
  // initialize DMC
#if (DMC_CALIBRATION_ENABLE == true)
  dmcInit(&_dmc, &_dmc_config, DME_TOPIC_ID, DMC_TARGETSERVICE_ID, DMC_CALIBSERVICE_ID, URT_THREAD_PRIO_RT_MAX);
#else
  dmcInit(&_dmc, &_dmc_config, DME_TOPIC_ID, DMC_TARGETSERVICE_ID, URT_THREAD_PRIO_RT_MAX);
#endif

  // initialize floor
  _floor.driver_data.frequency = frequency;
  floorInit(&_floor,
            AMBIENT_FLOOR_TOPICID,
            PROXIMITY_FLOOR_TOPICID,
            URT_THREAD_PRIO_NORMAL_MIN);

  //Subscriber subscribes to the proximity Floor Topic and should transmit to the floor id
  _can.sub_data[0].mask = TRIGGEREVENT;
  _can.sub_data[0].topicid = PROXIMITY_FLOOR_TOPICID;
  _can.sub_data[0].size = sizeof(floor_sensors_t);

  //Subscriber subscribes to the ambient Floor Topic and should transmit to the floor id
  _can.sub_data[1].mask = TRIGGEREVENT;
  _can.sub_data[1].topicid = AMBIENT_FLOOR_TOPICID;
  _can.sub_data[1].size = sizeof(floor_sensors_t);

  //Publisher gets data from the RING and publishs them to the RING Topic
  _can.pub_ring_data[0].topicid = PROXIMITY_RING_TOPICID;
  _can.pub_ring_data[0].size = sizeof(ring_sensors_t);

  _can.pub_ring_data[1].topicid = AMBIENT_RING_TOPICID;
  _can.pub_ring_data[1].size = sizeof(ring_sensors_t);

  //Request signals the motor service
  _can.request_data.serviceToSignal = &_dmc.service_targetspeed;

  id_map_t ids;
  ids.cnt = 0;
  ids.id = ~(_can.pub_ring_data[0].topicid ^ _can.pub_ring_data[1].topicid ^ DMC_SERVICE_ID); //ID must match
  ids.type = ~(CAN_REQUEST ^ CAN_TOPIC_DATA); //Type must match

  id_map_t filter_id;
  filter_id.cnt = 0;
  filter_id.id = _can.pub_ring_data[0].topicid | _can.pub_ring_data[1].topicid | DMC_SERVICE_ID;
  filter_id.type = CAN_REQUEST | CAN_TOPIC_DATA;

  // specify CAN Bus filter to get only for this app relevant messages
  _can.can_filter.mask.ext.id = ids.raw;
  _can.can_filter.mask.ext.ide = ~0;
  _can.can_filter.mask.ext.rtr = ~0;
  _can.can_filter.filter.ext.id = filter_id.raw;
  _can.can_filter.filter.ext.ide = CAN_IDE_EXT;
  _can.can_filter.filter.ext.rtr = CAN_RTR_DATA;
  _can.can_filter.cbparams = &_can;

  canLogicInit(&_can,
               URT_THREAD_PRIO_NORMAL_MIN);

#if (AMIROOS_CFG_SHELL_ENABLE == true) || defined(__DOXYGEN__)
  // add shell commands
  aosShellAddCommand(&_can_shellcmd);
  aosShellAddCommand(&_appsDmcSehhCmd);
  aosShellAddCommand(&_appsDmcShellCmd_getGains);
  aosShellAddCommand(&_appsDmcShellCmd_setGains);
#endif /* (AMIROOS_CFG_SHELL_ENABLE == true) || defined(__DOXYGEN__) */
  return;
}


