/*
AMiRo-Apps is a collection of applications for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2021  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <apps.h>
#include <ring.h>
#include <mazelogic.h>
#include <canbridge.h>
#include <stdlib.h>
#include <string.h>

/******************************************************************************/
/* LOCAL DEFINITIONS                                                          */
/******************************************************************************/

/******************************************************************************/
/* EXPORTED VARIABLES                                                         */
/******************************************************************************/

/******************************************************************************/
/* LOCAL TYPES                                                                */
/******************************************************************************/

/******************************************************************************/
/* LOCAL VARIABLES                                                            */
/******************************************************************************/

/*
 * forward declarations
 */
static int _ring_shellcb(BaseSequentialStream* stream, int argc, char* argv[]);
static int _can_shellcb(BaseSequentialStream* stream, int argc, char* argv[]);

/**
 * @brief   Shell command for proximity testing of CAN and ring.
 */
static AOS_SHELL_COMMAND(_ring_shellcmd, "RING:Test", _ring_shellcb);
static AOS_SHELL_COMMAND(_can_shellcmd, "CAN:Test", _can_shellcb);

/**
 * @brief Topic for the proximity/ambient ring data
 */
urt_topic_t _proximity_ring_topic;
urt_topic_t _ambient_ring_topic;
ring_sensors_t ring_proximity_payload;
ring_sensors_t ring_ambient_payload;

/**
 * @brief Topic for the floor data
 */
urt_topic_t _proximity_floor_topic;
urt_topic_t _ambient_floor_topic;
floor_sensors_t proximity_floor_payload;
floor_sensors_t ambient_floor_payload;

/**
 * @brief proximity driver frequency (in Hz).
 */
const float frequency = 50;

/**
 * @brief   Ring node object.
 */
static ring_node_t _ring;

/**
 * @brief CAN node object of the PowerManagement.
 */
static can_node_t _can;

/******************************************************************************/
/* LOCAL FUNCTIONS                                                            */
/******************************************************************************/

static int _ring_shellcb(BaseSequentialStream* stream, int argc, char* argv[])
{
  (void)argc;
  (void)argv;
  getProximityTest(stream, &_ring);
  return AOS_OK;
}

static int _can_shellcb(BaseSequentialStream* stream, int argc, char* argv[])
{
  (void)argc;
  (void)argv;

  for (int sensor = 0; sensor < 4; sensor++) {
    chprintf(stream, "Floor proximity data from CAN %i: %u \t ambient %u\n",
             sensor,
             _can.pub_floor_data[0].floor_data[0].values.data[sensor],
             _can.pub_floor_data[1].floor_data[0].values.data[sensor]);
  }
  chprintf(stream, "Timestamp prox: %u \n", _can.pub_floor_data[0].timestamp[0]);
  chprintf(stream, "Timestamp amb: %u \n", _can.pub_floor_data[1].timestamp[0]);


  chprintf(stream, "CAN callback: %f \n", _can.callback_test);
  chprintf(stream, "Ring callback: %u \n", _ring.callback_test);
  return AOS_OK;
}

/******************************************************************************/
/* EXPORTED FUNCTIONS                                                         */
/******************************************************************************/

/**
 * @brief   Initializes all Apps from the PowerManagement.
 */
void appsInit(void)
{
  //initialize topics
  urtTopicInit(&_proximity_ring_topic, PROXIMITY_RING_TOPICID, &ring_proximity_payload);
  urtTopicInit(&_ambient_ring_topic, AMBIENT_RING_TOPICID, &ring_ambient_payload);
  urtTopicInit(&_proximity_floor_topic, PROXIMITY_FLOOR_TOPICID, &proximity_floor_payload);
  urtTopicInit(&_ambient_floor_topic, AMBIENT_FLOOR_TOPICID, &ambient_floor_payload);

  //Subscriber subscribes to the proximity ring Topic and should transmit to the ring id
  _can.sub_data[0].mask = TRIGGEREVENT;
  _can.sub_data[0].topicid = PROXIMITY_RING_TOPICID;
  _can.sub_data[0].size = sizeof(ring_sensors_t);

  //Subscriber subscribes to the ambient ring Topic and should transmit to the ring id
  _can.sub_data[1].mask = TRIGGEREVENT;
  _can.sub_data[1].topicid = AMBIENT_RING_TOPICID;
  _can.sub_data[1].size = sizeof(ring_sensors_t);

  //Publisher gets data from the floor and publishs them to the floor Topic
  _can.pub_floor_data[0].topicid = PROXIMITY_FLOOR_TOPICID;
  _can.pub_floor_data[0].size = sizeof(floor_sensors_t);

  _can.pub_floor_data[1].topicid = AMBIENT_FLOOR_TOPICID;
  _can.pub_floor_data[1].size = sizeof(floor_sensors_t);

  //Service gets light request from the ring
  _can.light_service_data.id = LIGHT_SERVICE_ID;
  //Service gets motor request from the ring
  _can.motor_service_data.id = DMC_SERVICE_ID;

  id_map_t ids;
  ids.cnt = 0;
  ids.id = ~(_can.pub_floor_data[0].topicid ^ _can.pub_floor_data[1].topicid); //ID must match
  ids.type = ~0; //Type must match

  id_map_t filter_id;
  filter_id.cnt = 0;
  filter_id.id = _can.pub_floor_data[0].topicid | _can.pub_floor_data[1].topicid;
  filter_id.type = CAN_TOPIC_DATA;

  // specify CAN Bus filter to get only for this app relevant messages
  _can.can_filter.mask.ext.id = ids.raw; //Equal bit of these two masks must match other not
  _can.can_filter.mask.ext.ide = ~0;
  _can.can_filter.mask.ext.rtr = ~0;
  _can.can_filter.filter.ext.id = filter_id.raw; // should only receive messages from this ID
  _can.can_filter.filter.ext.ide = CAN_IDE_EXT;
  _can.can_filter.filter.ext.rtr = CAN_RTR_DATA;
  _can.can_filter.cbparams = &_can;

  canLogicInit(&_can,
               URT_THREAD_PRIO_NORMAL_MIN);

  // set the frequency
  _ring.driver_data.frequency = frequency;
  _ring.light_serviceToSignal = &_can.light_service_data.service;
  _ring.motor_serviceToSignal = &_can.motor_service_data.service;
  ringInit(&_ring,
           AMBIENT_RING_TOPICID,
           PROXIMITY_RING_TOPICID,
           URT_THREAD_PRIO_NORMAL_MIN);

  // add shell commands
  aosShellAddCommand(&_ring_shellcmd);
  aosShellAddCommand(&_can_shellcmd);

  return;
}
