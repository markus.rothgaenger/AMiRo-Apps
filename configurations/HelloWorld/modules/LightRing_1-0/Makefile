################################################################################
# AMiRo-Apps is a collection of applications for the Autonomous Mini Robot     #
# (AMiRo) platform.                                                            #
# Copyright (C) 2018..2021  Thomas Schöpping et al.                            #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
#                                                                              #
# This research/work was supported by the Cluster of Excellence Cognitive      #
# Interaction Technology 'CITEC' (EXC 277) at Bielefeld University, which is   #
# funded by the German Research Foundation (DFG).                              #
################################################################################



################################################################################
# Build global options                                                         #
# NOTE: Can be overridden externally.                                          #
#                                                                              #

# Compiler options here.
ifeq ($(USE_OPT),)
  USE_OPT = -O2 -fstack-usage -Wl,--print-memory-usage
  export USE_OPT
endif

# C specific options here (added to USE_OPT).
ifeq ($(USE_COPT),)
  USE_COPT = -std=c99 -fshort-enums
  export USE_COPT
endif

# C++ specific options here (added to USE_OPT).
ifeq ($(USE_CPPOPT),)
  USE_CPPOPT = -fno-rtti -std=c++17
  export USE_CPPOPT
endif

# Enable this if you want the linker to remove unused code and data.
ifeq ($(USE_LINK_GC),)
  USE_LINK_GC = yes
 export USE_LINK_GC
endif

# Linker extra options here.
ifeq ($(USE_LDOPT),)
  USE_LDOPT =
  export USE_LDOPT
endif

# Enable this if you want link time optimizations (LTO).
ifeq ($(USE_LTO),)
  USE_LTO = yes
  export USE_LTO
endif

# If enabled, this option allows to compile the application in THUMB mode.
ifeq ($(USE_THUMB),)
  USE_THUMB = yes
  export USE_THUMB
endif

# Enable this if you want to see the full log while compiling.
ifeq ($(USE_VERBOSE_COMPILE),)
  USE_VERBOSE_COMPILE = no
  export USE_VERBOSE_COMPILE
endif

#                                                                              #
# Build global options                                                         #
################################################################################

################################################################################
# Sources and paths                                                            #
#                                                                              #

# environment setup
include ../modules.mk

# include apps
include $(APPS_DIR)/HelloWorld/HelloWorld.mk

# middleware setup
include $(MIDDLEWARE_DIR)/middleware.mk

# C sources
APPS_CSRC += $(HelloWorld_CSRC) \
             $(MIDDLEWARE_CSRC)

# C++ sources
APPS_CPPSRC += $(HelloWorld_CPPSRC) \
               $(MIDDLEWARE_CPPSRC)

# include directories for configurations
APPS_INC += $(realpath .) \
            $(HelloWorld_INC) \
            $(MIDDLEWARE_INC)

#                                                                              #
# Sources and paths                                                            #
################################################################################

################################################################################
# Start of user section                                                        #
#                                                                              #

# List all user defines here
UDEFS +=
export UDEFS

# List all ASM defines here
UADEFS +=
export UADEFS

# List all user directories here
UINCDIR +=
export UINCDIR

# List all directories to look for user libraries here
ULIBDIR +=
export ULIBDIR

# List all user libraries here
ULIBS +=
export ULIBS

#                                                                              #
# End of user defines                                                          #
################################################################################

################################################################################
# Start of targets section                                                     #
#                                                                              #

# set the build directory
ifeq ($(BUILDDIR),)
  BUILDDIR = $(realpath .)/build
endif
export BUILDDIR

# call Makefile from OS
all:
	$(MAKE) -C $(OS_DIR)/AMiRo-OS/modules/LightRing_1-0/

clean:
	$(MAKE) -C $(OS_DIR)/AMiRo-OS/modules/LightRing_1-0/ clean

flash:
	$(MAKE) -C $(OS_DIR)/AMiRo-OS/modules/LightRing_1-0/ flash

#                                                                              #
# End of targets section                                                       #
################################################################################
